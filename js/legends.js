/*-----------------------------------------------------------------------------

	MML2 PC Mesh Viewer Copyright 2018 DashGL Project 

	MML2 PC Mesh Viewer is free software: you can redistribute it and/or modify 
	it under the terms of the GNU General Public License as published by the 
	Free Software Foundation, either version 3 of the License, or (at your 
	option) any later version.

	MML2 PC Mesh Viewer is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
	or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
	more details.

	You should have received a copy of the GNU General Public License along with
	MML2 PC Mesh Viewer. If not, see http://www.gnu.org/licenses/.

------------------------------------------------------------------------------*/

THREE.MML2Loader = function(texture) {

	this.texture = texture;
	this.lod = "high_lod";
	this.scale = 0.05;
	this.hasBones = false;
	this.bones = [];
	this.geometry = new THREE.Geometry();
	this.vertex_ofs = 0;
	this.geometry.faceVertexUvs[0] = [];
	this.geometry.animations = [];
	this.hierarchy = [];
	this.mat = [new THREE.MeshNormalMaterial()];

}

THREE.MML2Loader.prototype = {

	constructor : THREE.MML2Loader,

	parse : function (fp, model) {
		
		this.fp = fp;
		this.mesh_ofs = model.mesh_ofs;
		this.anim_tracks = model.anim_tracks;
		this.anim_control = model.anim_control;

		this.readMeshHeader();
		this.readMeshBones();
		this.readMeshHierarchy();
		this.readMeshPrimitives();
		this.readMeshMaterials();
		this.readMeshAnimations();

		this.geometry.computeFaceNormals();
	
		if(this.mat.length === 1) {
			for(let i = 0; i < this.geometry.faces.length; i++) {
				this.geometry.faces[i].materialIndex = 0;
			}
		}

		if(!this.hasBones) {
			this.mesh = new THREE.Mesh(this.geometry, this.mat);
		} else {
			for(let i = 0; i < this.mat.length; i++) {
				this.mat[i].skinning = true;
			}
			this.mesh = new THREE.SkinnedMesh(this.geometry, this.mat);
			let armSkeleton = new THREE.Skeleton(this.bones);
			this.mesh.add(armSkeleton.bones[0]);
			this.mesh.bind(armSkeleton);
		}
		this.mesh.name = model.flags.toString(16);
		this.mesh.rotation.y = Math.PI;
		return this.mesh;

	},

	readMeshHeader : function() {

		this.ofs = this.mesh_ofs;

		this.primitive_count = {
			high_lod : this.fp.getUint8(this.ofs + 0),
			med_lod : this.fp.getUint8(this.ofs + 1),
			low_lod : this.fp.getUint8(this.ofs + 2)
		}

		this.header = {
			high_lod : this.fp.getUint32(this.ofs + 0x04, true),
			med_lod : this.fp.getUint32(this.ofs + 0x08, true),
			low_lod : this.fp.getUint32(this.ofs + 0x0c, true),
			bone_ofs : this.fp.getUint32(this.ofs + 0x10, true),
			hierarchy_ofs : this.fp.getUint32(this.ofs + 0x14, true),
			texture_ofs : this.fp.getUint32(this.ofs + 0x18, true),
			bounding_box : this.fp.getUint32(this.ofs + 0x1c, true),
			brightness_ofs : this.fp.getUint32(this.ofs + 0x20, true)
		}

	},

	readMeshBones : function() {

		if(!this.header.bone_ofs) {
			this.bones[0] = new THREE.Bone();
			this.bones[0].name = 0;
			return;
		}

		let len = this.header.hierarchy_ofs - this.header.bone_ofs;
		let nb_bones = Math.floor(len / 6);
		this.bones = new Array(nb_bones);

		this.ofs = this.header.bone_ofs;
		for(let i = 0; i < nb_bones; i++) {
			
			let bone = new THREE.Bone();
			bone.name = i;
			bone.userData = {
				vertices : []
			}
			var x = this.fp.getInt16(this.ofs + 0, true);
			var y = this.fp.getInt16(this.ofs + 2, true);
			var z = this.fp.getInt16(this.ofs + 4, true);

			this.ofs += 6;
			
			bone.position.x = x * this.scale;
			bone.position.y = -y * this.scale;
			bone.position.z = z * this.scale;

			bone.updateMatrix();
			
			this.bones[i] = bone;
		}
		
		this.hasBones = true;

	},

	readMeshHierarchy : function() {

		if(!this.header.hierarchy_ofs) {
		
			let primitve_count = this.primitive_count[this.lod];
			for(let i = 0; i < primitve_count; i++) {
				this.hierarchy[i] = {
					draw : true,
					bone : this.bones[0]
				}
			}
			
			return;
		}

		this.ofs = this.header.hierarchy_ofs;
		let nbSegments = (this.header.texture_ofs - this.header.hierarchy_ofs) / 4;
		let share = false;

		for(let i = 0; i < nbSegments; i++) {
			
			let polygon_no = this.fp.getUint8(this.ofs + 0);
			let parent_bone = this.fp.getUint8(this.ofs + 1);
			let child_bone = this.fp.getUint8(this.ofs + 2);
			let polygon_flags = this.fp.getUint8(this.ofs + 3);

			this.ofs += 4;
			
			this.hierarchy[polygon_no] = {
				draw : true,
				bone : this.bones[child_bone]
			}
			
			if(polygon_no === 0) {
				continue;
			}

			if(polygon_flags & 0x80) {
				this.hierarchy[polygon_no].draw = false;
			}
			
			if(polygon_flags & 0x40) {
				share = true;
				this.hierarchy[polygon_no].share = true;
			} else {
				this.hierarchy[polygon_no].share = false;
			}

			if(this.bones[child_bone].parent) {
				continue;
			}

			if(parent_bone === child_bone) {
				continue;
			}
			
			this.bones[parent_bone].add(this.bones[child_bone]);

		}

		for(let i = 0; i < this.bones.length; i++) {
			this.bones[i].updateMatrix();
			this.bones[i].updateMatrixWorld();
		}

	},

	readMeshPrimitives : function() {
		
		this.ofs = this.header[this.lod];
		let primitve_count = this.primitive_count[this.lod];
		
		let prims = [];

		for(let i = 0; i < primitve_count; i++) {

			let prim = {
				id : i,
				nb_tri : this.fp.getUint8(this.ofs + 0),
				nb_quad : this.fp.getUint8(this.ofs + 1),
				nb_vert : this.fp.getUint8(this.ofs + 2),
				scale : this.fp.getUint8(this.ofs + 3),
				tri_ofs : this.fp.getUint32(this.ofs + 4, true),
				quad_ofs : this.fp.getUint32(this.ofs + 8, true),
				vert_ofs : this.fp.getUint32(this.ofs + 12, true)
			}
			
			this.ofs += 16;

			if(!this.hierarchy[i].draw) {
				continue;
			}

			prim.share = this.hierarchy[i].share;
			prim.bone = this.hierarchy[i].bone;
			prim.name = prim.bone.name;
			prim.scale = Math.pow(2, prim.scale);
			prims.push(prim);

		}
		
		prims.forEach(prim => {
			
			let draw = [7, 8];
			if(draw.indexOf(prim.id) === -1) {
				return;
			}

			console.log("Prim.share: ", prim.share);

			this.ofs = prim.vert_ofs;
			this.readMeshVertexList(prim.nb_vert, prim.scale, prim.bone, prim.share);

			this.ofs = prim.tri_ofs;
			this.readMeshFace(prim.nb_tri, false, prim.share)

			this.ofs = prim.quad_ofs;
			this.readMeshFace(prim.nb_quad, true, prim.share);
			
			this.previous_ofs = this.vertex_ofs;
			this.vertex_ofs += prim.nb_vert;

		});

	},

	readMeshFace : function(nbFace, isQuad, share) {
		
		const FACE_MASK = 0b1111111;

		for(let i = 0; i < nbFace; i++) {

			let auv = new THREE.Vector2(
				this.fp.getUint8(this.ofs + 0) / 255,
				this.fp.getUint8(this.ofs + 1) / 255
			);

			let buv = new THREE.Vector2(
				this.fp.getUint8(this.ofs + 2) / 255,
				this.fp.getUint8(this.ofs + 3) / 255
			);

			let cuv = new THREE.Vector2(
				this.fp.getUint8(this.ofs + 4) / 255,
				this.fp.getUint8(this.ofs + 5) / 255
			);

			let duv = new THREE.Vector2(
				this.fp.getUint8(this.ofs + 6) / 255,
				this.fp.getUint8(this.ofs + 7) / 255
			);

			let dword = this.fp.getUint32(this.ofs + 8, true);
			let a = (dword & FACE_MASK);
			let b = ((dword >> 7) & FACE_MASK);
			let c = ((dword >> 14) & FACE_MASK);
			let d = ((dword >> 21) & FACE_MASK);
			let matIndex = (dword >> 28) & 0b0011;
			this.ofs += 12;

			a += this.vertex_ofs;
			b += this.vertex_ofs;
			c += this.vertex_ofs;
			d += this.vertex_ofs;
			
			let face;
			face = new THREE.Face3(a, b, c);
			face.materialIndex = matIndex;
			
			this.geometry.faces.push(face);
			this.geometry.faceVertexUvs[0].push([auv, buv, cuv]);

			if(!isQuad) {
				continue;
			}

			face = new THREE.Face3(b,d,c);
			face.materialIndex = matIndex;
			
			this.geometry.faces.push(face);
			this.geometry.faceVertexUvs[0].push([buv, duv, cuv]);

		}

	},

	readMeshVertexList : function(nbVertex, scale, bone, share) {

		const VERTEX_MASK = 0b1111111111;
		const VERTEX_MSB = 0b1000000000;
		const VERTEX_LOW = 0b0111111111;
		let list = new THREE.Geometry();

		for(let i = 0; i < nbVertex; i++) {
			
			let dword = this.fp.getUint32(this.ofs, true);
			this.ofs += 4;

			let x = dword & VERTEX_MASK;
			let y = (dword >> 10) & VERTEX_MASK;
			let z = (dword >> 20) & VERTEX_MASK;
			let w = dword >> 30;
			let vertex = new THREE.Vector3();

			if (x & VERTEX_MSB) {
				x = (VERTEX_MSB - (x & VERTEX_LOW)) * -1;
			}

			if (y & VERTEX_MSB) {
				y = (VERTEX_MSB - (y & VERTEX_LOW)) * -1;
			}

			if (z & VERTEX_MSB) {
				z = (VERTEX_MSB - (z & VERTEX_LOW)) * -1;
			}

			x *= this.scale * scale;
			y *= -this.scale * scale;
			z *= this.scale * scale;

			vertex.x = x;
			vertex.y = y;
			vertex.z = z;
			vertex.applyMatrix4(bone.matrixWorld);
			vertex.bone = bone.name;
			vertex.index = this.geometry.vertices.length;

			let hasWeight = false;
			let prevBone = 0;
			
			if(share) {
				
				for(let k = this.previous_ofs; k < this.geometry.vertices.length; k++) {
					
					let prev = this.geometry.vertices[k];
					
					if(prev.x > vertex.x + 0.5 || prev.x < vertex.x - 0.5) {
						continue;
					}

					if(prev.y > vertex.y + 0.5 || prev.y < vertex.y - 0.5) {
						continue;
					}
					
					if(prev.z > vertex.z + 0.5 || prev.z < vertex.z - 0.5) {
						continue;
					}
					

					hasWeight = true;
					prevBone = prev.bone;
					
					let skinIndex = new THREE.Vector4(prev.bone, bone.name, 0, 0);
					let skinWeight = new THREE.Vector4(0.8, 0.2, 0, 0);
					this.geometry.skinIndices[prev.index] = skinIndex;
					this.geometry.skinWeights[prev.index] = skinWeight;

					break;
				}

			}

			if(this.hasBones) {
				
				if(!hasWeight) {
					vertex.indice = new THREE.Vector4(bone.name, 0, 0, 0);
					vertex.weight = new THREE.Vector4(1.0, 0, 0, 0)
				} else {
					console.log("setting to parent bone");
					vertex.indice = new THREE.Vector4(bone.name, prevBone, 0, 0);
					vertex.weight = new THREE.Vector4(0.8, 0.2, 0, 0)
				}

				this.geometry.skinIndices.push(vertex.indice);
				this.geometry.skinWeights.push(vertex.weight);
			}
			
			list.vertices.push(vertex);
			this.geometry.vertices.push(vertex);

		}
		
		let points = new THREE.Points(list);
		scene.add(points);

	},

	readMeshMaterials : function() {
		
		// If no texture exists, return

		if(!this.texture) {
			return;
		}
		
		let end = this.fp.buffer.byteLength;

		for(let key in this.header) {
			
			if(this.header[key] <= this.header.texture_ofs) {
				continue;
			}
			
			if(this.header[key] < end) {
				end = this.header[key];
			}

		}

		// If length values doesn't make sense, return

		let len = end - this.header.texture_ofs
		if(len <= 0 || len > 16 || len % 4) {
			return;
		}

		let nbMaterial = len / 4;
		let texture_list = new Array(nbMaterial);
		this.ofs = this.header.texture_ofs;
	
		for(let i = 0; i < nbMaterial; i++) {
			
			let image_coords = this.fp.getUint16(this.ofs + 0, true);
			let pallet_coords = this.fp.getUint16(this.ofs + 2, true);
			this.ofs += 4;
			
			texture_list[i] = {
				index : i,
				image : {
					x : (image_coords & 0x0f) << 6,
					y : image_coords & 0x10 ? 256 : 0
				},
				pallet : {
					x : (pallet_coords & 0x3f) << 4,
					y : pallet_coords >> 6
				}
			}

		}

		let assets = [];
		
		// Get all of the pallets and images in the file

		let view = this.texture.data;
		let asset_ofs = 0x800;
		for(this.ofs = 0; this.ofs < 0x800; this.ofs += 16) {
			
			let asset = {
				magic_number : view.getUint32(this.ofs, true),
				length : view.getUint32(this.ofs + 4, true),
				params : [
					view.getUint32(this.ofs + 8, true),
					view.getUint32(this.ofs + 12, true)
				],
				offset : asset_ofs
			}
			
			switch(asset.magic_number) {
			case 3:
			case 4:
				assets.push(asset);
				break;
			}

			asset_ofs += asset.length;
		}

		// Split the assets into pallets and images

		let pallets = [];
		let images = [];

		assets.forEach(asset => {
			
			this.ofs = asset.offset;
			pallets.push({
				x : view.getUint16(this.ofs + 4, true), 
				y : view.getUint16(this.ofs + 6, true),
				ofs : this.ofs
			});

			if(asset.magic_number !== 4) {
				return;
			}
			
			let len = view.getUint32(this.ofs, true);
			this.ofs += len;
			
			images.push({
				x : view.getUint16(this.ofs + 4, true), 
				y : view.getUint16(this.ofs + 6, true),
				width : view.getUint16(this.ofs + 8, true),
				height : view.getUint16(this.ofs + 10, true),
				ofs : this.ofs
			});

		});

		// Loop over texture list, try and find pallet and image for each texture

		for(let i = 0; i < texture_list.length; i++) {
			
			let tex = texture_list[i];
			let found = false;
			
			for(let k = 0; k < pallets.length; k++) {
				
				if(pallets[k].x !== tex.pallet.x) {
					continue;
				}
				
				if(pallets[k].y !== tex.pallet.y) {
					continue;
				}
				
				found = true;
				tex.pallet = pallets[k];
			}

			if(!found) {
				return;
			}

			let tx = tex.image.x;
			let ty = tex.image.y;
			tex.image = [];

			for(let k = 0; k < images.length; k++) {
				let x_ofs = images[k].x - tx;	
				let y_ofs = images[k].y % 256;

				if(ty === 0 && images[k].y >= 256) {
					continue;
				} else if(ty === 256 && images[k].y < 256) {
					continue;
				}

				if(x_ofs >= 64 || x_ofs < 0) {
					continue;
				}
				
				images[k].x_ofs = x_ofs;
				tex.image.push(images[k]);

				if(images[k].height === 256) {
					break;
				}
			}

			if(!tex.image.length) {
				return;
			}

		}

		// Pallets and images have been found, next step is to render
		
		let mat = new Array();
		
		texture_list.forEach(tex => {
			
			let canvas = document.createElement("canvas");
			canvas.width = 256;
			canvas.height = 256;
			let ctx = canvas.getContext("2d");
			
			this.ofs = tex.pallet.ofs;
			let pallet_len = view.getUint32(this.ofs + 0, true);
			let pallet_x = view.getUint16(this.ofs + 4, true);
			let pallet_y = view.getUint16(this.ofs + 6, true);
			let pallet_colors = view.getUint16(this.ofs + 8, true);
			let pallet_count = view.getUint16(this.ofs + 10, true);
			this.ofs += 12;
			
			let pallet = new Array(pallet_len / 2);

			for(let i = 0; i < pallet_len; i++) {
				
				let color = view.getUint16(this.ofs, true);
				let r = ((color >> 0x00) & 0x1f) << 3;
				let g = ((color >> 0x05) & 0x1f) << 3;
				let b = ((color >> 0x0a) & 0x1f) << 3;
				let a = color > 0 ? 1 : 0;

				pallet[i] = "rgba("+r+","+g+","+b+","+a+")";
				this.ofs += 2;
			
			}

			// Determine bytes per pixel

			let bpp = pallet_colors === 256 ? 8 : 4;
			
			tex.image.forEach(img => {
				
				this.ofs = img.ofs;
				let image_len = view.getUint32(this.ofs + 0, true);
				let image_x = view.getUint16(this.ofs + 4, true);
				let image_y = view.getUint16(this.ofs + 6, true);
				let image_width = view.getUint16(this.ofs + 8, true) * 2;
				let image_height = view.getUint16(this.ofs + 10, true);

				this.ofs += 12;
				image_len -= 12;
				
				if(bpp === 4) {
					image_width *= 2;
				}
				
				let image_body = new Array();

				for(let i = 0; i < image_len; i++) {
					let byte = view.getUint8(this.ofs++);

					switch(bpp) {
					case 8:
						image_body.push(pallet[byte]);
						break;
					case 4:
						image_body.push(pallet[(byte & 0xf)]);
						image_body.push(pallet[(byte >> 4)]);
						break;
					}

				}
				
				let y_ofs = img.y % 256;
				let x_ofs = img.x_ofs * 4;

				let k = 0;
				for(let y = 0; y < image_height; y++) {
					for(let x = 0; x < image_width; x++) {
						let color = image_body[k];
						ctx.fillStyle = color;
						ctx.fillRect(x + x_ofs, y + y_ofs, 1, 1);
						k++;
					}
				}
				

			});
			
			// Create texture
			
			let texture = new THREE.Texture(canvas);
			texture.flipY = false;
			texture.needsUpdate = true;

			mat.push( new THREE.MeshBasicMaterial({
				map : texture,
				alphaTest : 0.01,
				transparent : true
			}));

		});
		
		var blank = document.createElement('canvas');
		blank.width = 256;
		blank.height = 256;
		let url = blank.toDataURL();

		for(let i = 0; i < mat; i++) {
			if(mat.map.image.toDataURL() !== url) {
				continue;
			}
			return;
		}

		this.mat = mat;
	
	},

	readMeshAnimations : function () {

		const VERTEX_MASK = 0b1111111111;
		const VERTEX_MSB = 0b1000000000;
		const VERTEX_LOW = 0b0111111111;
		const ROT_MAGNITUDE = [ 90, 180, 360, 720 ];
		const POS_MAGNITUDE = [ 1, 2, 4, 8 ];

		if(!this.anim_tracks) {
			return;
		}

		let controls = [];
		let first_control_ofs = this.fp.getUint32(this.anim_control, true);
		for(this.ofs = this.anim_control; this.ofs < first_control_ofs; this.ofs += 4) {
			controls.push(this.fp.getUint32(this.ofs, true));
		}
		
		let tracks = [];
		let first_track_ofs = this.fp.getUint32(this.anim_tracks, true);
		for(this.ofs = this.anim_tracks; this.ofs < first_track_ofs; this.ofs += 4) {
			tracks.push(this.fp.getUint32(this.ofs, true));
		}

		let anim_num = 0;

		controls.forEach(control_ofs => {
			
			let anim_track = this.fp.getUint8(control_ofs + 0);
			let anim_len = this.fp.getUint8(control_ofs + 1);
			
			if(anim_len === 1) {
				return;
			}

			let track_ofs = tracks[anim_track];
			let stride = (this.bones.length + 1) * 4;
			
			let animation = {
				name : null,
				fps : 30,
				length : (anim_len - 1) / 30,
				hierarchy : []
			};

			for(var i = 0; i < this.bones.length; i++) {
				animation.hierarchy.push({
					parent : i - 1,
					keys : []
				});
			}

			for(var i = 0; i < anim_len; i++) {
				
				control_ofs += 4;
				let track_stride = this.fp.getUint8(control_ofs);
				this.ofs = track_ofs + (stride * track_stride);

				let dword = this.fp.getUint32(this.ofs, true);
				this.ofs += 4;

				let root_x = dword & VERTEX_MASK;
				let root_y = (dword >> 10) & VERTEX_MASK;
				let root_z = (dword >> 20) & VERTEX_MASK;
				let root_m = (dword >> 30) & 0b11;

				if (root_x & VERTEX_MSB) {
					root_x = (VERTEX_MSB - (root_x & VERTEX_LOW)) * -1;
				}

				if (root_y & VERTEX_MSB) {
					root_y = (VERTEX_MSB - (root_y & VERTEX_LOW)) * -1;
				}

				if (root_z & VERTEX_MSB) {
					root_z = (VERTEX_MSB - (root_z & VERTEX_LOW)) * -1;
				}
				
				let p_mag = POS_MAGNITUDE[root_m];

				let root_pos = [
					this.bones[0].position.x + root_x * this.scale * p_mag, 
					this.bones[0].position.y + root_y * -this.scale * p_mag, 
					this.bones[0].position.z + root_z * this.scale * p_mag
				];
			
				for(var k = 0; k < this.bones.length; k++) {
					
					let dword = this.fp.getUint32(this.ofs, true);
					this.ofs += 4;

					let x = dword & VERTEX_MASK;
					let y = (dword >> 10) & VERTEX_MASK;
					let z = (dword >> 20) & VERTEX_MASK;
					let w = (dword >> 30) & 0b11;
				
					// Positive Rotation

					let x_pos = (x & 0x200) / 0x3ff;
					let y_pos = -(y & 0x200) / 0x3ff;
					let z_pos = (z & 0x200) / 0x3ff;
					
					// Negative Rotation

					let x_neg = -(x & 0x1ff) / 0x3ff;
					let y_neg = (y & 0x1ff) / 0x3ff;
					let z_neg = -(z & 0x1ff) / 0x3ff;

					let mag = ROT_MAGNITUDE[w];

					let rot = {
						x : (x_pos + x_neg)*mag,
						y : (y_pos + y_neg)*mag,
						z : (z_pos + z_neg)*mag
					};

					let e = new THREE.Euler(
						rot.x * Math.PI / 180,
						rot.y * Math.PI / 180,
						rot.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);

					let key = {
						time : i / 30,
						rot : q.toArray(),
						scl : [1,1,1]
					}

					if(k === 0) {
						key.pos = root_pos;
					} else {
						key.pos = this.bones[k].position.toArray()
					}

					animation.hierarchy[k].keys.push(key);

				}

				// End rotation

			}
			
			let clip = THREE.AnimationClip.parseAnimation(animation, this.bones);
			if(!clip) {
				return;
			}

			clip.optimize();
			this.geometry.animations.push(clip);

		});

	}

}
