<!DOCTYPE HTML>
<html>

	<head>
		
		<title>Dash Model Format</title>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">	
		<link rel="stylesheet" type="text/css" href="css/layout.css"/>

	</head>

	<body>

		<nav>
			
			<div class="block">
				<img src="img/others_20171117_dashgl_dash_cropped_icon.png" width="144" height="244.8"/>
			</div>

			<ul>
				<li><a href="?attr=DMF">Header</a></li>
				<li><a href="?attr=ATTR">Attributes</a></li>
				<li><a href="?attr=NAME">Name List</a></li>
				<li><a href="?attr=BONE">Bone List</a></li>
				<li><a href="?attr=VERT">Vertex List</a></li>
				<li><a href="?attr=TEX">Texture List</a></li>
				<li><a href="?attr=MAT">Material List</a></li>
				<li><a href="?attr=FACE">Face Groups</a></li>
				<li><a href="?attr=ANIM">Animation List</a></li>
				<li><a href="?attr=PLUG">Plugin Support</a></li>
			</ul>

		</nav>

		<main>
			
			<header>
				
				<?php
					
					$attr = "DMF";
					
					if(isset($_GET["attr"])) {
						$attr = $_GET["attr"];
					}

					$obj = Array(
						"DMF" => "Header",
						"ATTR" => "Attributes",
						"NAME" => "Name List",
						"BONE" => "Bone List",
						"VERT" => "Vertex List",
						"TEX" => "Texture List",
						"MAT" => "Material List",
						"FACE" => "Face Groups",
						"ANIM" => "Animation List",
						"PLUG" => "Plugin Support"
					);

					$title = $obj[$attr];
					echo "Dash Model Format >> $title\n";

				?>

			</header>

			<section>

				<!--

					DMF

				-->

				<block class="<?php if($attr == "DMF") echo 'open'; ?>">
					
					<h4>File Declaration</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>DMF</td>
								<td>offset</td>
								<td>rev 1</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x0010</th>
								<td>&nbsp</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>0x0020</th>
								<td>ATTR</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num attr</td>
							</tr>
							<tr>
								<td>0x0030</th>
								<td>NAME</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num tex</td>
							</tr>
							<tr>
								<td>0x0040</th>
								<td>BONE</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num bone</td>
							</tr>
							<tr>
								<td>0x0050</th>
								<td>VERT</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num vert</td>
							</tr>
							<tr>
								<td>0x0060</th>
								<td>TEX</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x0070</th>
								<td>TEX</td>
								<td>offset</td>
								<td>id 1</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x0080</th>
								<td>MAT</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x0090</th>
								<td>MAT</td>
								<td>offset</td>
								<td>id 1</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x00a0</th>
								<td>FACE</td>
								<td>offset</td>
								<td>mat id</td>
								<td>num tri</td>
							</tr>
							<tr>
								<td>0x00b0</th>
								<td>FACE</td>
								<td>offset</td>
								<td>mat id</td>
								<td>num tri</td>
							</tr>
							<tr>
								<td>0x00c0</th>
								<td>FACE</td>
								<td>offset</td>
								<td>mat id</td>
								<td>num tri</td>
							</tr>
							<tr>
								<td>0x00d0</th>
								<td>ANIM</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num frames</td>
							</tr>
							<tr>
								<td>0x00e0</th>
								<td>ANIM</td>
								<td>offset</td>
								<td>id 1</td>
								<td>num frames</td>
							</tr>
							<tr>
								<td>0x00f0</th>
								<td>ANIM</td>
								<td>offset</td>
								<td>id 2</td>
								<td>num frames</td>
							</tr>
							<tr>
								<td>0x0100</th>
								<td>&nbsp;</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>

						</tbody>
					</table>

					<p>
					<b>DMF</b>
					</p>

					<p>
					The above table displays a sample header for the Dash Model Format.
					The first row of the file is standard. The first four bytes will always
					be the magic number DMF. The next four bytes is the offset
					to the start of the header list. Most often this will be 0x20,
					as there is a line of padding between the file declaration and the
					header list in case properties are added later on. It also allows
					for the option for multiple DMF models to be packed into a single file.
					</p>

					<p>
					The next four bytes of the file delcaration is the file version.
					Right now the DMF version is 1, and if things go well, this will
					be the maintained version. However if changes are made such that
					compatibility becomes impossible is implausible, this version will
					be incremented to 2. The last four bytes in the file declaration
					are the number of properties in the header list.
					</p>

					<p>
					The header list defines a list of <em>blocks</em> that define
					the model. Each block header has a length of 16 bytes starting
					with a magic number to describe the type of the block, followed
					by the offset inside the file to where the block is defined. The
					next four bytes is an id or reference. In most cases it defines
					an id for the block, in the face of face groups, it defines the
					material index being used for the group of faces. And the last
					byte bytes contains the number of elements defined inside the block.
					</p>
					
					<p>
					A short description of the header list definitions of each blocks
					will follow below. The definitions for the layout of each block
					is defined on their respective pages accessible from the left hand
					side bar.
					</p>

					<p>
					<b>ATTR</b>
					</p>

					<p>
					The magic number ATTR defines a list of attributes to be used globally
					inside the model. Specifically these are vertex weights, number of
					uv entries, existence of vertex colors and vertex normals. These are
					defined in a key-value list such that more properties can be defined
					later if needed as to not break compatibility. If no ATTR block is
					defined in the file, all default values will be assumed.
					</p>

					<p>
					<b>NAME</b>
					</p>

					<p>
					The magic number NAME defines a list of names. Specifically for the 
					texture and animation lists. The list is
					packed such that the end of each name string is terminated with '\0`.
					A list of empty names maybe in a file as a sequence of empty zero's.
					This block is not required and the exporter will check if names are
					set and omit if not pressent.
					</p>

					<p>
					<b>BONE</b>
					</p>

					<p>
					The magic number BONE defines the bone list. Only one bone list can
					be defined per file. If no bones are present this block is not needed.
					</p>

					<p>
					<b>VERT</b>
					</p>

					<p>
					The magic number VERT defines a vertex list. Only one vertex list
					can be defined per file. The DMF can be used to store a material list
					where only materials and textures are present. If no VERT block is
					found, the loader will assume a material list.
					</p>

					<p>
					<b>TEX</b>
					</p>

					<p>
					The magic number TEX defines a texture. A block will be defined for
					the number of textures that exist inside the model. Each texture comes
					with generally a set number of properties before the image is defined.
					</p>

					<p>
					<b>MAT</b>
					</p>

					<p>
					The magic number MAT defines a material. A block will be defined for
					the number of materials the exist inside the model. The id defines the
					material id, which is later references by a face group. Any textures
					referenced by the material will be defined in the key-value block.
					</p>

					<p>
					<b>FACE</b>
					</p>

					<p>
					The magic number FACE defines a face group. A block is defined for
					each 'group' within the model. Specifically a 'group' is a sequence
					of continuous triangles with the same material index. For instance
					if a motorcycle is defined with two wheels using the same material
					and the body using a different material, and the draw order is
					front wheel, body, back wheel. Then the model would be expressed
					as three 'groups', each existing as a FACE block.
					</p>

					<p>
					<b>ANIM</b>
					</p>

					<p>
					The magic number ANIM defines an skeletal animation. Each animation
					contains a list of key frames with the transformation matrix for 
					each bone in the frame. An ANIM block for each individual animation.
					</p>


				</block>

				<!--

					ATTR

				-->

				<block class="<?php if($attr == "ATTR") echo 'open'; ?>">
					
					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>ATTR</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num attr 4</td>
							</tr>
						</tbody>
					</table>
					
					<p>
					The table above shows the header entry for the attribute block.
					The first four bytes are the magic number. The second four bytes are the
					offset to the block. The third offset is the id of the attribute
					block, which should always be zero. And the last four bytes of the
					header definition should be the int 4, until new attributes are added
					into the definition. The attrbute block defines optional attributes
					than are used global across the model. If this block is not defined,
					then no optional values will be read, specifically meaning that
					vertices will not include weights, and triangles will only consist 
					of indices without any uv, vertex normals or vertex colors.
					</p>

					<h4>Block Definition</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>ATTR</td>
								<td>len</td>
								<td>VWGT</td>
								<td>bool</td>
							</tr>
							<tr>
								<td>0x0010</th>
								<td>UVCT</td>
								<td>int</td>
								<td>VNRM</td>
								<td>bool</td>
							</tr>
							<tr>
								<td>0x0020</th>
								<td>VCLR</td>
								<td>bool</td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above gives the layout for the body of the attribute block.
					The block starts with the magic number of the block, which is ATTR, 
					followed by the length of the data defined in the block.
					</p>

					<p>
					Currently the attributes defined in this block are as follows.
					
					<li>
					VWGT - "Vertex Weights" defines if skin indices and skin weights
					are included in the vertex list or not. 0 for no, 1 for yes.
					</li>

					<li>
					UVCT - "UV Count" defines the number of uv's defined in each face
					triangle. Currently the only values recognized by the importer and
					exporter are 0 and 1, but more uv values maybe added in the future,
					though they might be defined as "MAP" definitions. More research
					needs to be done in this area. Though if map is used this property
					will be switched from a UV to a boolean and defined as MAPT.
					</li>

					<li>
					VNRM - "Vertex Normals" definses if normals are attached to the 
					face definition or not. Normals are defined as x, y, z float values
					between 0.0 and 1.0.
					</li>

					<li>
					VCLR - "Vertex Colors" defines if colors are attached to the
					face definition or not. Colors are defines as r, b, g, float values
					between 0.0 and 1.0.
					</li>

					</p>

					<h4>Open Issues</h4>

					<p>
					Right now I'm debating the following issues for this section.

					<li>
					Do I need to define vertex weights in this section, or can i assume
					that vertex weights must be pressent in the vertex list. Or do I require
					the attribute to be set as a form of possitive confirmation?
					</li>

					<li>
					What properties do I need to define for uv's? Do i simply define a list
					of uv's to be stored for the user? Or do I create properties for the
					specific purposes of uv's?
					</li>

					<li>
					Is defining attributes globally for the entire model a good approach?
					Or should I allow face attributes to be set by group? One of the
					design goals of this file format is to allow models to be easily 
					converted to buffer geometry. And that means keeping the interface
					consistent across all faces. 
					</li>

					<li>
					So for now defining an attribute block means that I can add attributes
					into the header as needed, and keep them global across the model. And
					allow for the possibility of declaring multiple attribute blocks to
					change attributes if needed in the future. Right now only the first attribute
					block declared will be reconigized, and must come before any vertex
					or face lists.
					</li>

					</p>

				</block>

				<!--

					NAME

				-->

				<block class="<?php if($attr == "NAME") echo 'open'; ?>">
					
					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>NAME</td>
								<td>offset</td>
								<td>id 0</td>
								<td>bone name 13</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>NAME</td>
								<td>offset</td>
								<td>id 1</td>
								<td>tex name 2</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>NAME</td>
								<td>offset</td>
								<td>id 2</td>
								<td>anim name 3</td>
							</tr>
						</tbody>
					</table>
					
					<p>
					The table above shows the header entry for the name block. The first four bytes
					are the NAME magic number, followed by its offset in the file. The third set is
					the block id. And the last four bytes is the number of the property being defined.
					</p>
					
					<h4>Block Definition</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>NAME</td>
								<td>len</td>
								<td>TEX</td>
								<td>moto</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>rcyl</td>
								<td>e_wh</td>
								<td>eel.</td>
								<td>png</td>
							</tr>
							<tr>
								<td>0x0000</td>
								<td>\0mot</th>
								<td>orcy</td>
								<td>le_b</td>
								<td>ody.</td>
							</tr>
							<tr>
								<td>0x0000</td>
								<td>png\0</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					
					<p>
					The table above shows the structure of the name block. The block starts with
					the NAME magic number and the length of the content of the block.
					Following the magic and the length, the first data is a maguc number defining
					the name of the property being defined.
					</p>

					<p>
					Following the example of the motorcycle defined in the header, we have
					"motorcycle_wheel.png" and "motorcycle_body.png" for texture names. And
					"drive", "park", and "crash" for animation names. 
					</p>

					<p>
					If no NAME block is defined, then no names will be asigned to the attributes
					being parsed in the file. The exporter will look at the names of the attributes
					being exported, and if all are null or empty strings, the NAME block will
					not be generated for that attribute.
					</p>

					<p>
					Since names will have to be defined before their respective blocks, it's recomended
					to have them defined at the begining of the file. Names will not be retroactively
					applied.
					</p>

					<p>
					Strings will be parsed one at a time until a null byte is encountered. In this way
					a series of null characters can be represent emtpy strings. So in the case that only
					the second texture had a name, this block would be represented as "\0motorcycle_body.png\0".
					</p>

				</block>

				<!--

					BONE

				-->

				<block class="<?php if($attr == "BONE") echo 'open'; ?>">

					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>BONE</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num bone 3</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the format for the header entry of the BONE block.
					Only one bone block is allowed per DMF file. If a bone entry exists, the
					model will be parsed as a skinned mesh, otherwise the model will be parsed
					as a standard mesh. The first four bytes are the magic number BONE, the second
					four bytes are the pointer to the block location in the file. The third
					four bytes is the id 0. And last is the number of bones declared in the block.
					</p>

					<h4>Block Definition</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>BONE</td>
								<td>len</td>
								<td>id | parent id</td>
								<td>elem 0</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 1</th>
								<td>elem 2</td>
								<td>elem 3</td>
								<td>elem 4</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 5</th>
								<td>elem 6</td>
								<td>elem 7</td>
								<td>elem 8</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 9</th>
								<td>elem 10</td>
								<td>elem 11</td>
								<td>elem 12</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 13</th>
								<td>elem 14</td>
								<td>elem 15</td>
								<td>id | parent id</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 0</th>
								<td>elem 1</td>
								<td>elem 2</td>
								<td>elem 3</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 4</th>
								<td>elem 5</td>
								<td>elem 6</td>
								<td>elem 7</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 8</th>
								<td>elem 9</td>
								<td>elem 10</td>
								<td>elem 11</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 12</th>
								<td>elem 13</td>
								<td>elem 14</td>
								<td>elem 15</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>id | parent id</td>
								<td>elem 0</th>
								<td>elem 1</td>
								<td>elem 2</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 3</th>
								<td>elem 4</td>
								<td>elem 5</td>
								<td>elem 6</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 7</th>
								<td>elem 8</td>
								<td>elem 9</td>
								<td>elem 10</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 11</th>
								<td>elem 12</td>
								<td>elem 13</td>
								<td>elem 14</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>elem 15</th>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the block definition of the bone list. The block
					starts out with the magic number BONE and the length of the content defined
					within the bone. The first data is a signed short for the id, and a signed
					short for the parent id. In the case of the first bone, this will always be
					0 and -1. The short is signed in order to express the negaive number to 
					express the lack of a parent bone for the root bone.
					</p>

					<p>
					From there, there are 16 floats that make up a 4x4 transformation matrix
					that makes up the bone. The elements are stored in column-major order.
					After the end of the 16 elements have been declared, the next bone starts
					with signed shorts for the id and parent id, followed by 16 elements for
					the bone. This pattern continues for number of bones defined in the header.
					</p>

<pre>
struct DashBone {
	short id;
	short parent_id;
	float elements[16];
};
</pre>

				<h4>Open Issues</h4>

				<p>
				Another option would be to include the position, rotation and scale for
				each bone. But that would require rebuilding the bone from the elements,
				so simply having the elements seems like the most standard way to manage
				this information.
				</p>

				</block>

				<!--

					VERT

				-->

				<block class="<?php if($attr == "VERT") echo 'open'; ?>">

					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>VERT</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num vertex 64</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the header defition of the VERT block. The first four
					bytes are the magic number VERT, the second four bytes is the offset inside
					the file. The third four bytes is the id of 0. And the last four bytes are the
					number of vertices defined in the file.
					</p>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>VERT</td>
								<td>len</td>
								<td>x</td>
								<td>y</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>z</td>
								<td>indice 0 | 1</td>
								<td>indice 2 | 3</td>
								<td>weight 0</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>weight 1</td>
								<td>weight 2</td>
								<td>weight 3</td>
								<td>x</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>y</td>
								<td>z</td>
								<td>indice 0 | 1</td>
								<td>indice 2 | 3</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>weight 0</td>
								<td>weight 1</td>
								<td>weight 2</td>
								<td>...</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows a sample of the VERT block. The block starts with
					the VERT magic number, and is followed by the length of the data declared
					within the block. The vertex list then has two possible formats depending
					on if the VWGT or "vertex weight" flag is set in the ATTR block inside the
					file.
					</p>

					<p>
					In the case that there is either no ATTR block, or an ATTR block has been
					defined and the VWGT flags is not set, then the vertex list will simply
					be a list of x, y, z vertices defined as a list of 32 bit floats.
					</p>

					<p>
					In the case that the ATTR block exists and the VWGT flags is set then the
					vertex list has the structure of x, y, z 32 bit floats for each vertex
					followed by four unsigned short indices, followed by four 32 bit floats 
					for vertex weights.
					</p>

					<p>
					Note that the number of weights is always 4 and there is no flag to set
					the number of weights. If no weights exist for a given vertex, or there
					are less than four weights are required, simply enter 0 for the indice
					and 0 for the weight.
					</p>
					
					<pre>
struct DashVertex {
	float x,y,z
}

struct DashVertexWeight {
	unsigned short indices[4];
	float weights[4];
}
					</pre>

					<h4>Open Issues</h4>

					<p>
					Originally I was going to include a number of weights per vertex option.
					But threejs and gltf have weights defined as vec4 data types. So to keep
					consistency and reduce potential confusion I decided to force four weights
					even if it means a slightly longer file. Though in addition to the VWGT
					"vertex weights" yes or no flag, another flag can be introduced for the
					number of weights, with the default of four set, without breaking backwards
					compatibility.
					</p>

				</block>

				<!--

					Tex

				-->

				<block class="<?php if($attr == "TEX") echo 'open'; ?>">

					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>TEX</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>TEX</td>
								<td>offset</td>
								<td>id 1</td>
								<td>num props</td>
							</tr>
						</tbody>
					</table>
					
					<p>
					The table above shows the header format of the TEX block. A TEX block
					defines a texture, and properties associated with it. The first four bytes
					is the magic number TEX, the second four bytes are the pointer to the location
					of the offset. The third four bytes is a unique integer id for the texture.
					Not that the number displayed is not the one actually used. The texture id
					is forced by the order in which the textures are listed in the file. Materials
					refer to textures by these numbers for mapping. As such these numbers are
					to be used as a reference while viewing the file.
					</p>

					<p>
					The last four bytes are the number of properties defined in the file. The number
					of properties will always be at least three, as the extension, the length of the
					image and the actual image data always need to be defined. Other properties like
					flipy and properties for texture repeat can be set here too.
					</p>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>TEX</td>
								<td>len</td>
								<td>TYPE</td>
								<td>png</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>LEN</td>
								<td>unsigned int</td>
								<td>FLPY</td>
								<td>bool</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>WID</td>
								<td>unsgined int</td>
								<td>HGT</td>
								<td>unsigned int</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>WRPS</td>
								<td>unsigned int</td>
								<td>WRPT</td>
								<td>unsigned int</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>IMG</td>
								<td>img data</td>
								<td>-</td>
								<td></td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the block definition of the TEX block. The first four
					bytes are the magic number TEX followed by the length of data defined inside
					the block. Following the block label is a list of properties that can come in 
					any order with one exception, the IMG property comes last.
					</p>

					<p>
					The texture block defines a list of properties that define a texture. These
					properties include:

					<li>TYPE (required) - the extension of the image, right now png format is forced in the
					importer and exporter, but ttf, jpeg, or otherwise could be included later.</li>
					
					<li>LEN (required) - the byte length of the IMG defined in this block.</li>
					
					<li>FLPY - "flipY" property, follwed by a boolean 1 for yes, 0 for no value.</li>

					<li>WID - width of the image in pixels.</li>

					<li>HGT - height of the image in pixels</li>

					<li>WRPS - wrapS This defines how the texture is wrapped horizontally and corresponds 
					to U in UV mapping. The default is THREE.ClampToEdgeWrapping, where the edge is clamped 
					to the outer edge texels. other two choices are THREE.RepeatWrapping and 
					THREE.MirroredRepeatWrapping.</li>

					<li>WRPT - wrapT This defines how the texture is wrapped vertically and corresponds 
					to V in UV mapping. The default is THREE.ClampToEdgeWrapping, where the edge is clamped 
					to the outer edge texels. other two choices are THREE.RepeatWrapping and 
					THREE.MirroredRepeatWrapping.</li>
					</p>

					<li>
					IMG - The actual image data. Currently a base64 data url string. Could be switched
					to binary later.
					</li>
					
					<h4>Open Issues</h4>

					<p>
					Originally I had just wanted to declare the image directly without any properties
					but I realized that wrapping, filtering, and sampling it would be better to
					have a list of properties to preceed the image to be on the safe side.
					</p>

					<p>
					I am tempted to make the option of adding in global properties into the ATTR
					block to set default texture properties so they don't have to be defined everytime.
					Though that would make the parses and exporter code comre complicated. And in
					general use, not a lot can be assumed about which options are global, so
					setting them for each image seems to be the safe bet.
					</p>

					<p>
					Right now I'm forcing png with a limited number of properties. Allowing for 
					more image types and defining more properties like filtering, maybe something
					I have to implement later. But for testing I'll keep it simple. Also, not sure
					about introducing mipmaps or not.
					</p>

				</block>

				<!--

					MAT

				-->

				<block class="<?php if($attr == "MAT") echo 'open'; ?>">

					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>MAT</td>
								<td>offset</td>
								<td>id 0</td>
								<td>num props</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>MAT</td>
								<td>offset</td>
								<td>id 1</td>
								<td>num props</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the  header definition for the MAT block. The first 
					four bytes is the magic number MAT. The second four bytes is the offset
					inside the file. The third four bytes is the material id. This will be
					a sequential number of integers starting from 0. Face groups will refer
					to this number. And the last four bytes is the number of properties.
					</p>

					<h4>Block Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>MAT</td>
								<td>len</td>
								<td>DIFF</td>
								<td>r</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>g</td>
								<td>b</td>
								<td>MAP0</td>
								<td>tex id</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>TYPE</td>
								<td>basi</td>
								<td>c\0</td>
								<td></td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>OPAC</td>
								<td>float</td>
								<td>ALPT</td>
								<td>float</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>VCLR</td>
								<td>int</td>
								<td>PREC</td>
								<td>enum</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>DITH</td>
								<td>bool</td>
								<td>SIDE</td>
								<td>int</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>TRNS</td>
								<td>bool</td>
								<td>VSBL</td>
								<td>bool</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the definition of the MAT block. The block starts off
					with the magic number MAT and is followed by the length of the data defined
					in the block. The block itself is made of key value pairs. Properties of the
					material are defined with magic numbers follows by the value data that defines it.
					</p>

					<p>
					In most cases the values will be defined as four byte ints. However depending
					on the property, they can be longer. Such as the DIFF or "diffuse" property
					which is followed by three 32 bit floats, one for red, green and blue respectively.
					The properties as as defined below.

					<li>
					DIFF - "diffuse" diffuse color of the material, followed by three floats for
					r, g, b/
					</li>

					<li>
					MAP0 - The texture id which is used with the uv0 coords.
					</li>

					<li>
					TYPE - the type of the material, defined as a 12 byte string. Possible values
					are "basic", "phong", "lambert".
					</li>

					<li>
					OPAC - The opacity of the material defined as a 32 bit float between 0.0 and 1.0.
					</li>

					<li>
					ALPT - Alphatest, defined as a 32 bit float bewteen 0.0 and 1.0.
					</li>

					<li>
					VCLR - Vertex color default is THREE.NoColors otherwise THREE.VertexColors
					</li>

					<li>
					PREC - Float precision values are "low", "med" or "high"
					</li>

					<li>
					DITH - Dithering, true defined as 1, false defined as 0
					</li>

					<li>
					SIDE -Defines which side of faces will be rendered - front, back or both. 
					Default is THREE.FrontSide. Other options are THREE.BackSide and THREE.DoubleSide. 
					</li>

					<li>
					TRNS - Transparent, true is defined as 1, false defined as 0
					</li>

					<li>
					VSBL - Visible, true is defined as 1, false defined as 0
					</li>

					</p>

					<h4>Open Issues</h4>

					<p>
					Materials are a rabbit hole, so right now I only have a few properties
					defined for basic use. More properties will need to be defined with 
					a magic number for each one and a definition. There's also more material
					types, but I'll keep it simple and slowly expand out.
					</p>

					<p>
					The other issue is how to handle texture mapping. Right now I have
					the number of uv's defined on each face as an int. That gives me
					a couple of options to define how textures. Do i defined them
					as MAP0, MAP1, MAP2 for which texture to use a specific UV array.
					Or do I make it more specific, MAPT, MAPE, MAPN for "map texture",
					"map environment", "map normals". I could do this and also define
					a number after each ne for the uv reference and then another one
					for the texture id reference.
					</p>

				</block>

				<!--

					FACE

				-->

				<block class="<?php if($attr == "FACE") echo 'open'; ?>">
					
					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>FACE</td>
								<td>offset</td>
								<td>mat id 0</td>
								<td>num indices 21</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>FACE</td>
								<td>offset</td>
								<td>mat id 1</td>
								<td>num indices 66</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>FACE</td>
								<td>offset</td>
								<td>mat id 0</td>
								<td>num indices 21</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the header entry for the FACE block. The first four bytes
					are the magic number FACE, the second four bytes are the offset inside the file.
					The third four bytes is the material index id for the face group. And the last
					our bytes is the number of indices defined inside the block. Indices represent 
					points on a triangle, so the number should always be divisble by three.
					</p>
					
					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>FACE</td>
								<td>len</td>
								<td>a indice</td>
								<td>u0</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>v0</td>
								<td>normal x</td>
								<td>normal y</td>
								<td>normal z</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>vcolor r</td>
								<td>vcolor b</td>
								<td>vcolor g</td>
								<td>b indice</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>u0</td>
								<td>v0</td>
								<td>normal x</td>
								<td>normal y</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>normal z</td>
								<td>vcolor r</td>
								<td>vcolor b</td>
								<td>vcolor g</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>c indice</td>
								<td>u0</td>
								<td>v0</td>
								<td>normal x</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>normal y</td>
								<td>normal z</td>
								<td>vcolor r</td>
								<td>vcolor b</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>vcolor g</td>
								<td>...</td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the structure of the FACE block. The first four bytes
					are the bagic number FACE, followed by the length of the data defined inside
					block. The block data starts with the a indice, which is actually a two byte
					unsigned short. It it displayed in the table above as a 4 byte value for the
					sake of formatting.
					</p>

					<p>
					What information follows deoends on the flags declared in the ATTR block. If
					no flags are set, then this list will simply be a list of indices. The three
					attributes that affect this block are UVCT "uv count", VNRM "vertex normal"
					and VCLR "vertex color".
					</p>

					<pre>
[ unsigned short Indice ] 
( UVCT [ float U0 ] [float V0] ) 
(VNRM [float x] [float y][float z] )  
(VCLR [float r] [float g][float b] )</pre>

					<p>
					The text above shows what the above table looks like. Following the indices,
					if the UVCT attribute is above 1, then that number of uv pairs will follow
					the indice. In this case there is only one UV declared in the uv count, but 
					if two were declared, they would follow in the order of u0, v0, u1, v1. 
					</p>

					<p>
					If set, the next value will be the vertex normals. The vertex normals
					are a float for x, y, and z respectively. Note that face normals are not
					included in this definition as they are not attached to the vertex, and
					can be calculated with "calculateFaceNormals".
					</p>

					<p>
					If set, the next value will be vertex colors. The vertex colors are a float
					for r, g, and b between 0 and 1. This pattern is then repeated for each
					indice with every three indices forming a triangle. So the order is
					a0, b0, c0, a1, b1, c1, with the properties for each indice following each
					indice.
					</p>

					<h4>Open Issues</h4>

					<p>
					When creating this format, I went for indice specific to create a repeating
					pattern. But after writing this I'm tempted to change the definition to a 
					triangle centric representation. Of a0, b0, c0, followed by a list of uv'v,
					followed by the normals, followed by the colors. That would be a lot
					easier to export and parse. And it would make the number of triangles easier
					to use in the header. This could also be done with minimal changes to the code.
					Though it actually works pretty well now, and changes to how parsing is done
					could be added to the ATTR block later.
					</p>

				</block>

				<!--

					ANIM

				-->

				<block class="<?php if($attr == "ANIM") echo 'open'; ?>">
					
					<h4>Header Definitions</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>ANIM</td>
								<td>offset</td>
								<td>anim id 0</td>
								<td>num frames 21</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>ANIM</td>
								<td>offset</td>
								<td>anim id 1</td>
								<td>num frames 43</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>ANIM</td>
								<td>offset</td>
								<td>anim id 2</td>
								<td>num frames 32</td>
							</tr>
						</tbody>
					</table>
					
					<p>
					The table above shows the format for the animation header entry. The first
					four bytes are the magic number ANIM. The second four bytes are the offset
					inside the file. The third four bytes show the animation id number, which 
					should be a sequential list of integers. Note that the order of animations
					defines the animation id, and these numbers are here for display purposes.
					If the animation names are set in a NAME block, then names will be added
					according to the order they appear in. And the last four bytes are the total
					number of keyframes in the file. Which is not used, but available for verfication
					purposes.
					</p>
					
					<h4>Block Definition</h4>

					<table border="1">
						<thead>
							<tr>
								<th></th>
								<th>0x00</th>
								<th>0x04</th>
								<th>0x08</th>
								<th>0x0c</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0x0000</th>
								<td>ANIM</td>
								<td>len</td>
								<td>duration float</td>
								<td>parent -1</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>num key frames 2</th>
								<td>time 0</td>
								<td>'prs"</td>
								<td>pos0</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>pos1</td>
								<td>pos2</td>
								<td>rot0</td>
								<td>rot1</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>rot2</td>
								<td>rot3</td>
								<td>scl0</td>
								<td>scl1</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>scl2</td>
								<td>time 1</td>
								<td>'pr"</td>
								<td>pos0</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>pos1</td>
								<td>pos2</td>
								<td>rot0</td>
								<td>rot1</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>rot2</td>
								<td>rot3</td>
								<td>parent 0</td>
								<td>num key frames</td>
							</tr>
							<tr>
								<td>0x0000</th>
								<td>'prs'</td>
								<td>pos0</td>
								<td>pos1</td>
								<td>...</td>
							</tr>
						</tbody>
					</table>

					<p>
					The table above shows the description of the animation block. It starts with 
					the magic number ANIM, followed by the length of the data defined inside the
					block. 
					</p>

					<p>
					The first value defined inside the block is the duration, defined as a float
					value. Following that will be a list of key frames for each bone starting with
					bone 0. So while not actually needed, for confirmation, the following value
					is a signed int for the bone parent number. After the parent number is the 
					number of key frames given for that bone. In this example we'll go with two.
					</p>

					<p>
					Following the definition is a float value given for the time, followed by the
					key frame format, which will consist of the characters 'prs'. The 'prs'
					characters define which transformations are being defined for the frame. If
					'p' exists, that means position is defined, and three float for the postion
					will be included. If 'r' exists it means that four floats for the rotation
					quarternion will be defined. If 's' exists, it means that three floats for
					scale will be defined.
					</p>

					<p>
					Any combination of 'prs' is possible. 'p' only, 'r' only, 's' only, 'prs',
					'pr' and 'rs'. But a character will not be repeated and will always be declared
					in the order of p first, then r, then s in the case they exist.
					</p>

					<p>
					So the 'prs' format is read. Which means 12 bytes for position, 16 bytes for 
					rotation, and 12 bytes for scale. Following the keyframe will be the time
					index for the next keyframe, followed by the next 'prs' format, which will
					give the format for the next key frame. This loop for the number of keyframes
					defined after the parent bone number. 
					</p>

					<p>
					This will repeat for each parent bone for the number of bones in the bone list.
					So starting with -1, 0, and up until the number of bones has been reached.
					First the bone number is declared, followed by the number of frames. Each frame
					is given a time index, followed by the 'prs' format, followed by the data defined.
					</p>
					
					<h4>Open Issues</h4>

					<p>
					Originally I had wanted to use transformation matrices, but didn't get good
					results with the matrix decope method to get the individual transformations
					back, and ended up going with the separated elements to begin with. The difference
					in size for each entry isn't hard to deal with, using the 'prs' definition header.
					</p>

				</block>
				
				<!--

					THREE
				
				-->

				<block class="<?php if($attr == "PLUG") echo 'open'; ?>">

					<h4 id="#three">Blender</h4>

					<li>Import Plugin</li>
					<li>Load from server</li>
					<li>Load from file</li>
					<li>Load from indexeddb</li>

					<li>Export Plugin</li>
					<li>Example Page</li>
					
					<h4 id="#blender">Blender</h4>

				</block>


				<p style="text-align: center">
					Copyright &copy 2018 DashGL Project
				</p>

			</section>

		</main>

		<footer>

		</footer>

	</body>

</html>
