/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2018 Benjamin Collins (kion @ dashgl.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/


THREE.DashExporter = function() {

	this.attrs = {
		"vertex_weight" : false,
		"uv_count" : 0,
		"vertex_normals" : false,
		"vertex_colors" : false
	}

	this.names = {
		"bone" : [],
		"tex" : [],
		"anim" : []
	}

    this.bones = [];
	this.vertices = [];
	this.textures = [];
	this.materials = [];
	this.groups = [];
	this.anims = [];

	this.blocks = [];

}

THREE.DashExporter.calcBufferLen = function(byteLen) {
		
	let bufferLen;
	let remainder = byteLen % 16;

	if(remainder === 8) {
		bufferLen = byteLen + 8;
	} else if(remainder < 8) {
		bufferLen = byteLen + (16 - remainder);
	} else {
		bufferLen = byteLen + (16 - remainder) + 16;
	}
		
	return bufferLen;

}

THREE.DashExporter.prototype = {

    constructor : THREE.DashExporter,

    parse : function(mesh) {

		// Read Input

		this.readAttributes(mesh.geometry);
		this.readBones(mesh.skeleton);
		this.readVertices(mesh.geometry);
		this.readTextures(mesh.material);	
		this.readMaterials(mesh.material);	
		this.readFaceGroups(mesh.geometry);
		this.readAnimations(mesh.geometry);

		// Write blocks
		
		this.writeAttributes();
		this.writeNames();
		this.writeBones();
		this.writeVertices();
		this.writeTextures();
		this.writeMaterials();
		this.writeFaceGroups();
		this.writeThreeAnimations();
		//this.writeMatrixAnimations();
		this.writeHeader();
		
		// Return blob

		return this.pack();

    },

	readAttributes : function(geometry) {

		let face = geometry.faces[0] || {};
		let indice = geometry.skinIndices.length;
		let weight = geometry.skinWeights.length;

		// Set attributes
		
		this.attrs.vertex_weight = (indice && weight) ? 1 : 0;
		this.attrs.uv_count = geometry.faceVertexUvs.length || 0;
		this.attrs.vertex_normals = face.vertexNormals.length ? 1 : 0;
		this.attrs.vertex_colors = face.vertexColors.length ? 1 : 0;

	},

    readBones : function(skeleton) {

		if(!skeleton) {
			return;
		}

		// Populate bones

		for(let i = 0; i < skeleton.bones.length; i++) {
			
			let bone = {
				id : i,
				elements : skeleton.bones[i].matrix.elements
			};
			
			this.names.bone.push(skeleton.bones[i].name);
			bone.parent = skeleton.bones.indexOf(skeleton.bones[i].parent);
			this.bones.push(bone);

		}

    },

	readVertices : function(geometry) {

		// Read all of the vertices
		
		for(let i = 0; i < geometry.vertices.length; i++) {

			let vertex = {
				pos : {
					x : geometry.vertices[i].x, 
					y : geometry.vertices[i].y,
					z : geometry.vertices[i].z
				}
			};

			if(this.attrs.vertex_weight) {
				
				vertex.indice = {
					x : geometry.skinIndices[i].x,
					y : geometry.skinIndices[i].y,
					z : geometry.skinIndices[i].z,
					w : geometry.skinIndices[i].w
				}
				
				vertex.weight = {
					x : geometry.skinWeights[i].x,
					y : geometry.skinWeights[i].y,
					z : geometry.skinWeights[i].z,
					w : geometry.skinWeights[i].w
				}

			}

			this.vertices.push(vertex);

		}

	},

	readTextures : function(material) {

		// Check if single material is used

		if(!Array.isArray(material)) {
			material = [material];
		}

		// Get texture for each material

		for(let i = 0; i < material.length; i++) {
			
			if(!material[i].map) {
				continue;
			}

			let uuid = material[i].map.uuid;
			let found = false;
			
			for(let k = 0; k < this.textures.length; k++) {
				if(this.textures[k].uuid !== uuid) {
					continue;
				}
				found = true;
				break;
			}

			if(found) {
				continue;
			}
			
			let canvas, ctx;
			
			let tex = material[i].map;

			switch(tex.image.tagName) {
			case "IMG":
				canvas = document.createElement("canvas");
				canvas.width = tex.image.width;
				canvas.height = tex.image.height;
				ctx = canvas.getContext("2d");
				ctx.drawImage(tex.image, 0, 0);
				break;
			case "CANVAS":
				canvas = tex.image;
				break;
			default:
				console.error("Switch case could not be matched");
				break;
			}
			
			this.textures.push({
				uuid : uuid,
				img : canvas,
				flipY : tex.flipY,
				width : canvas.width,
				height : canvas.height,
				wrapS : tex.wrapS,
				wrapT : tex.wrapT
			});
			
			this.names["tex"].push(tex.name);

		}


	},

	readMaterials : function(material) {

		// Check if single material is used

		if(!Array.isArray(material)) {
			material = [material];
		}

		// Get texture for each material

		for(let i = 0; i < material.length; i++) {
			
			// Default Diffuse

			let mat = {};
			
			// Set type

			switch(material[i].type) {
			case "MeshBasicMaterial":
				mat.type = "basic";
				break;
			case "MeshLambertMaterial":
				mat.type = "lambert";
				break;
			case "MeshPhongtMaterial":
				mat.type = "phong";
				break;
			}

			// If diffuse, set color

			if(material[i].color) {
				mat.diffuse = {
					r : material[i].color.r,
					g : material[i].color.g,
					b : material[i].color.b
				};
			}

			// If texture, set index reference

			if(material[i].map) {
				
				let uuid = material[i].map.uuid;

				for(let k = 0; k < this.textures.length; k++) {
					if(this.textures[k].uuid !== uuid) {
						continue;
					}
					mat.map = k;
					break;
				}

			}

			// Check opacity

			if(typeof material[i].opacity !== 'undefined') {
				mat.opacity = typeof material[i].opacity;
			}

			// Check skinning

			if(typeof material[i].skinning !== 'undefined') {
				mat.skinning = typeof material[i].skinning;
			}

			// Check alphaTest

			if(typeof material[i].alphaTest !== 'undefined') {
				mat.alphaTest = typeof material[i].alphaTest;
			}
			
			// Set vertex colors

			mat.vertexColors = typeof material[i].vertexColors;
			mat.side = typeof material[i].side;
			mat.transparent = typeof material[i].transparent ? 1 : 0;
		

			this.materials.push(mat);

		}

	},

	readFaceGroups : function(geometry) {

		// Loop over the faces

		let group;
		let matId = -1;

		for(let i = 0; i < geometry.faces.length; i++) {

			let face = geometry.faces[i];
			let matIndex = face.materialIndex;

			if(matId !== matIndex) {
				if(group) {
					this.groups.push(group);
				}
				
				matId = matIndex;
				group = {
					matIndex : matIndex,
					tri : []
				};
			}

			let a = { 
				index : face.a
			}

			let b = {
				index : face.b
			}

			let c = {
				index : face.c
			}

			// Read vertex uv's

			if(this.attrs.uv_count) {
				
				a.uv = new Array(this.attrs.uv_count);
				b.uv = new Array(this.attrs.uv_count);
				c.uv = new Array(this.attrs.uv_count);

			}

			for(let k = 0; k < this.attrs.uv_count; k++) {
				
				if(!geometry.faceVertexUvs[k][i][0]) {

					a.uv[k] = {
						u : NaN,
						v : NaN
					}
				
					b.uv[k] = {
						u : NaN,
						v : NaN
					}
				
					c.uv[k] = {
						u : NaN,
						v : NaN
					}

					continue;
				}

				a.uv[k] = {
					u : geometry.faceVertexUvs[k][i][0].x,
					v : geometry.faceVertexUvs[k][i][0].y
				}
				
				b.uv[k] = {
					u : geometry.faceVertexUvs[k][i][1].x,
					v : geometry.faceVertexUvs[k][i][1].y
				}
				
				c.uv[k] = {
					u : geometry.faceVertexUvs[k][i][2].x,
					v : geometry.faceVertexUvs[k][i][2].y
				}

			}

			if(this.attrs.vertex_normals) {

				a.normal = {
					x : face.vertexNormals[0].x,
					y : face.vertexNormals[0].y,
					z : face.vertexNormals[0].z
				}

				b.normal = {
					x : face.vertexNormals[1].x,
					y : face.vertexNormals[1].y,
					z : face.vertexNormals[1].z
				}

				c.normal = {
					x : face.vertexNormals[2].x,
					y : face.vertexNormals[2].y,
					z : face.vertexNormals[2].z
				}

			}

			if(this.attrs.vertex_colors) {

				a.vcolor = {
					r : face.vertexColors[0].r,
					g : face.vertexColors[0].g,
					b : face.vertexColors[0].b
				}

				b.vcolor = {
					r : face.vertexColors[1].r,
					g : face.vertexColors[1].g,
					b : face.vertexColors[1].b
				}

				c.vcolor = {
					r : face.vertexColors[2].r,
					g : face.vertexColors[2].g,
					b : face.vertexColors[2].b
				}

			}

			group.tri.push(a, b, c);

		}

		this.groups.push(group);

	},

	readAnimations : function(geometry) {

		if(!geometry.animations || !geometry.animations.length) {
			return;
		}
		
		for(let i = 0; i < geometry.animations.length; i++) {
		
			this.names.anim.push(geometry.animations[i].name);
			
			let blockSize = 4;

			let parent = -1;
			let tracks = geometry.animations[i].tracks;
			
			let animation = {
				name : geometry.animations[i].name,
				length : geometry.animations[i].duration,
				hierarchy : []
			};

			for(let k = 0; k < this.bones.length; k++) {
				
				blockSize += 8;

				let pos = tracks[k * 3 + 0];
				let rot = tracks[k * 3 + 1];
				let scl = tracks[k * 3 + 2];

				let time = {};

				for(let j = 0; j < pos.times.length; j++) {
					let key = pos.times[j].toString();
					time[key] = time[key] || {};
					time[key].pos = [
						pos.values[j * 3 + 0],
						pos.values[j * 3 + 1],
						pos.values[j * 3 + 2]
					];
				}

				for(let j = 0; j < rot.times.length; j++) {
					let key = rot.times[j].toString();
					time[key] = time[key] || {};
					time[key].rot = [
						rot.values[j * 4 + 0],
						rot.values[j * 4 + 1],
						rot.values[j * 4 + 2],
						rot.values[j * 4 + 3]
					];
				}

				for(let j = 0; j < scl.times.length; j++) {
					let key = scl.times[j].toString();
					time[key] = time[key] || {};
					time[key].scl = [
						scl.values[j * 3 + 0],
						scl.values[j * 3 + 1],
						scl.values[j * 3 + 2]
					];
				}

				let keys = Object.keys(time);
				keys.sort(function(a, b) {
					return parseFloat(a) - parseFloat(b);
				});
				
				var hierarchy = new Array(keys.length);
				blockSize += keys.length * 8;

				for(let j = 0; j < keys.length; j++) {
					
					let frame = time[keys[j]];
					frame.time = keys[j];
						
					let mat4 = new THREE.Matrix4();
					
					if(frame.rot) {
						let r = frame.rot;
						let q = new THREE.Quaternion(r[0], r[1], r[2], r[3]);
						mat4.makeRotationFromQuaternion(q);
						blockSize += 16;
					}
					
					if(frame.scl) {
						let s = frame.scl;
						let v = new THREE.Vector3(s[0],s[1],s[2]);
						mat4.scale(v);
						blockSize += 12;
					}
					
					if(frame.pos) {
						let p = frame.pos;
						let v = new THREE.Vector3(p[0],p[1],p[2]);
						mat4.setPosition(v);
						blockSize += 12;
					}

					frame.elements = mat4.elements;
					hierarchy[j] = frame;
				}
				
				animation.hierarchy.push({
					parent : parent,
					keys : hierarchy
				});

				parent++;
			}
			
			animation.blockSize = blockSize;
			this.anims.push(animation);

		}

	},

	writeAttributes : function() {

		let keys = Object.keys(this.attrs);
		let byteLen = keys.length * 8;
		let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);
			
		let buffer = new ArrayBuffer(bufferLen);
		let view = new DataView(buffer);
	
		view.setUint8(0, 'A'.charCodeAt(0));
		view.setUint8(1, 'T'.charCodeAt(0))
		view.setUint8(2, 'T'.charCodeAt(0));
		view.setUint8(3, 'R'.charCodeAt(0));
		view.setUint32(4, byteLen, true);

		let ofs = 8;
		for(let i = 0; i < keys.length; i++) {
			
			let val;

			switch(keys[i]) {
			case "vertex_weight":
				view.setUint8(ofs + 0, 'V'.charCodeAt(0));
				view.setUint8(ofs + 1, 'W'.charCodeAt(0))
				view.setUint8(ofs + 2, 'G'.charCodeAt(0));
				view.setUint8(ofs + 3, 'T'.charCodeAt(0));
				val = this.attrs[keys[i]] ? 1 : 0;
				view.setUint32(ofs + 4, val, true);
				break;
			case "uv_count":
				view.setUint8(ofs + 0, 'U'.charCodeAt(0));
				view.setUint8(ofs + 1, 'V'.charCodeAt(0))
				view.setUint8(ofs + 2, 'C'.charCodeAt(0));
				view.setUint8(ofs + 3, 'T'.charCodeAt(0));
				val = this.attrs[keys[i]];
				view.setUint32(ofs + 4, val, true);
				break;
			case "vertex_normals":
				view.setUint8(ofs + 0, 'V'.charCodeAt(0));
				view.setUint8(ofs + 1, 'N'.charCodeAt(0))
				view.setUint8(ofs + 2, 'R'.charCodeAt(0));
				view.setUint8(ofs + 3, 'M'.charCodeAt(0));
				val = this.attrs[keys[i]] ? 1 : 0;
				view.setUint32(ofs + 4, val, true);
				break;
			case "vertex_colors":
				view.setUint8(ofs + 0, 'V'.charCodeAt(0));
				view.setUint8(ofs + 1, 'C'.charCodeAt(0))
				view.setUint8(ofs + 2, 'L'.charCodeAt(0));
				view.setUint8(ofs + 3, 'R'.charCodeAt(0));
				val = this.attrs[keys[i]] ? 1 : 0;
				view.setUint32(ofs + 4, val, true);
				break;
			}

			ofs += 8;
		}

		this.blocks.push({
			type : "ATTR",
			num : keys.length,
			buffer : buffer
		});

	},

	writeNames : function() {
		
		let id = 0;

		for(let key in this.names) {

			if(!this.names[key].length) {
				continue;
			}
			
			var found = false;
			let list = new Array(this.names[key].length);
	
			for(let i = 0; i < this.names[key].length; i++) {
				let val = this.names[key][i].toString();
				let name = val.replace('\0', '');
				list[i] = name;

				if(!name.length) {
					continue;
				}

				found = true;
				break;
			}
			
			if(!found) {
				continue;
			}
			

			let str = this.names[key].join("\0") + "\0";
			let byteLen = str.length + 4;
			let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);

			let buffer = new ArrayBuffer(bufferLen);
			let view = new DataView(buffer);
	
			view.setUint8(0, 'N'.charCodeAt(0));
			view.setUint8(1, 'A'.charCodeAt(0));
			view.setUint8(2, 'M'.charCodeAt(0));
			view.setUint8(3, 'E'.charCodeAt(0));
			view.setUint32(4, byteLen, true);
			
			switch(key) {
			case "tex":
				view.setUint8(8, 't'.charCodeAt(0));
				view.setUint8(9, 'e'.charCodeAt(0));
				view.setUint8(10, 'x'.charCodeAt(0));
				break;
			case "anim":
				view.setUint8(8, 'a'.charCodeAt(0));
				view.setUint8(9, 'n'.charCodeAt(0));
				view.setUint8(10, 'i'.charCodeAt(0));
				view.setUint8(11, 'm'.charCodeAt(0));
				break;
			case "bone":
				view.setUint8(8, 'b'.charCodeAt(0));
				view.setUint8(9, 'o'.charCodeAt(0));
				view.setUint8(10, 'n'.charCodeAt(0));
				view.setUint8(11, 'e'.charCodeAt(0));
				break;
			}
			
			let ofs = 12;
			for(let i = 0; i < str.length; i++) {
				view.setUint8(ofs + i, str.charCodeAt(i));
			}

			this.blocks.push({
				type : "NAME",
				num : list.length,
				buffer : buffer
			});

		}

	},

	writeBones : function() {
		
		if(!this.bones.length) {
			return;
		}

		// First read the bones list

		let structSize = (16 * 4 + 4);
		let byteLen = this.bones.length * structSize;
		let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);
			
		let buffer = new ArrayBuffer(bufferLen);
		let view = new DataView(buffer);
	
		view.setUint8(0, 0x42); // 'B'
		view.setUint8(1, 0x4F); // '0'
		view.setUint8(2, 0x4E); // 'N'
		view.setUint8(3, 0x45); // 'E'
		view.setUint32(4, byteLen, true);
		
		let ofs = 8;
		for(let i = 0; i < this.bones.length; i++) {

			view.setInt16(ofs, this.bones[i].id, true);
			view.setInt16(ofs + 2, this.bones[i].parent, true);
			ofs += 4;
			
			this.bones[i].elements.forEach(e => {
				view.setFloat32(ofs, e, true);
				ofs += 4;
			});

		}
		
		this.blocks.push({
			type : "BONE",
			num : this.bones.length,
			buffer : buffer
		});

	},

	writeVertices : function() {
		
		let structSize = 12;
		
		if(this.bones.length) {
			structSize += 2*4 + 4*4;
		}

		let byteLen = this.vertices.length * structSize;
		let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);

		let buffer = new ArrayBuffer(bufferLen);
		let view = new DataView(buffer);
	
		view.setUint8(0, 0x56); // 'V'
		view.setUint8(1, 0x45); // 'E'
		view.setUint8(2, 0x52); // 'R'
		view.setUint8(3, 0x54); // 'T'
		view.setUint32(4, byteLen, true);
		
		let ofs = 8;
		for(let i = 0; i < this.vertices.length; i++) {

			view.setFloat32(ofs + 0, this.vertices[i].pos.x, true);
			view.setFloat32(ofs + 4, this.vertices[i].pos.y, true);
			view.setFloat32(ofs + 8, this.vertices[i].pos.z, true);
			ofs += 12;

			if(!this.bones.length) {
				continue;
			}
			
			view.setUint16(ofs + 0, this.vertices[i].indice.x, true);
			view.setUint16(ofs + 2, this.vertices[i].indice.y, true);
			view.setUint16(ofs + 4, this.vertices[i].indice.z, true);
			view.setUint16(ofs + 6, this.vertices[i].indice.w, true);
			ofs += 8;
			
			view.setFloat32(ofs + 0, this.vertices[i].weight.x, true);
			view.setFloat32(ofs + 4, this.vertices[i].weight.y, true);
			view.setFloat32(ofs + 8, this.vertices[i].weight.z, true);
			view.setFloat32(ofs + 12, this.vertices[i].weight.w, true);
			ofs += 16;

		}

		this.blocks.push({
			type : "VERT",
			buffer : buffer,
			num : this.vertices.length
		});

	},

	writeTextures : function() {

		for(let i = 0; i < this.textures.length; i++) {
			
			let tex = this.textures[i];
			let canvas = this.textures[i].img;
			let data = canvas.toDataURL("image/png");
			let index = data.indexOf(",") + 1;
			data = data.substr(index);
			data = atob(data);

			let byteLen = data.length;
			let structSize = byteLen + 4;

			tex.type = "PNG";
			tex.len = byteLen;
			let props = 1;
			let prop = {};

			for(let key in tex) {
				switch(key) {
				case "type":
				case "len":
				case "flipY":
				case "width":
				case "height":
				case "wrapS":
				case "wrapT":
					prop[key] = true;
					props++;
					structSize += 8;
					break;
				}
			}

			let bufferLen = THREE.DashExporter.calcBufferLen(structSize);
			let buffer = new ArrayBuffer(bufferLen);
			let view = new DataView(buffer);
	
			view.setUint8(0, 0x54); // 'T'
			view.setUint8(1, 0x45); // 'E'
			view.setUint8(2, 0x58); // 'X'
			view.setUint8(3, 0);    // '\0'
			view.setUint32(4, structSize, true);

			let ofs = 8;

			if(prop.type) {
				view.setUint8(ofs + 0, 'T'.charCodeAt(0));
				view.setUint8(ofs + 1, 'Y'.charCodeAt(0));
				view.setUint8(ofs + 2, 'P'.charCodeAt(0));
				view.setUint8(ofs + 3, 'E'.charCodeAt(0));
				ofs += 4;
				for(let k = 0; k < tex.type.length; k++) {
					view.setUint8(ofs + k, tex.type.charCodeAt(k));
				}
				ofs += 4;
			}

			if(prop.len) {
				view.setUint8(ofs + 0, 'L'.charCodeAt(0));
				view.setUint8(ofs + 1, 'E'.charCodeAt(0));
				view.setUint8(ofs + 2, 'N'.charCodeAt(0));
				view.setUint8(ofs + 3, '\0'.charCodeAt(0));
				view.setUint32(ofs + 4, byteLen, true);
				ofs += 8;
			}


			if(prop.flipY) {
				let flipY = tex.flipY ? 1 : 0;
				view.setUint8(ofs + 0, 'F'.charCodeAt(0));
				view.setUint8(ofs + 1, 'L'.charCodeAt(0));
				view.setUint8(ofs + 2, 'P'.charCodeAt(0));
				view.setUint8(ofs + 3, 'Y'.charCodeAt(0));
				view.setUint32(ofs + 4, flipY, true);
				ofs += 8;
			}

			if(prop.width) {
				view.setUint8(ofs + 0, 'W'.charCodeAt(0));
				view.setUint8(ofs + 1, 'I'.charCodeAt(0));
				view.setUint8(ofs + 2, 'D'.charCodeAt(0));
				view.setUint8(ofs + 3, '\0'.charCodeAt(0));
				view.setUint32(ofs + 4, tex.width, true);
				ofs += 8;
			}

			if(prop.width) {
				view.setUint8(ofs + 0, 'H'.charCodeAt(0));
				view.setUint8(ofs + 1, 'G'.charCodeAt(0));
				view.setUint8(ofs + 2, 'T'.charCodeAt(0));
				view.setUint8(ofs + 3, '\0'.charCodeAt(0));
				view.setUint32(ofs + 4, tex.width, true);
				ofs += 8;
			}

			if(prop.wrapS) {
				view.setUint8(ofs + 0, 'W'.charCodeAt(0));
				view.setUint8(ofs + 1, 'R'.charCodeAt(0));
				view.setUint8(ofs + 2, 'P'.charCodeAt(0));
				view.setUint8(ofs + 3, 'S'.charCodeAt(0));
				view.setUint32(ofs + 4, tex.wrapS, true);
				ofs += 8;
			}

			if(prop.wrapT) {
				view.setUint8(ofs + 0, 'W'.charCodeAt(0));
				view.setUint8(ofs + 1, 'R'.charCodeAt(0));
				view.setUint8(ofs + 2, 'P'.charCodeAt(0));
				view.setUint8(ofs + 3, 'T'.charCodeAt(0));
				view.setUint32(ofs + 4, tex.wrapT, true);
				ofs += 8;
			}

			view.setUint8(ofs + 0, 'I'.charCodeAt(0));
			view.setUint8(ofs + 1, 'M'.charCodeAt(0));
			view.setUint8(ofs + 2, 'G'.charCodeAt(0));
			view.setUint8(ofs + 3, '\0'.charCodeAt(0));
			ofs += 4;

			for(let i = 0; i < byteLen; i++) {
				view.setUint8(ofs, data.charCodeAt(i));
				ofs++;
			}

			this.blocks.push({
				type : "TEX",
				id : i,
				num : props,
				buffer : buffer
			});

		}

	},

	writeMaterials : function() {

		for(let i = 0; i < this.materials.length; i++) {

			let byteLen = 0;
			let properties = Object.keys(this.materials[i]);

			for(let key in this.materials[i]) {
				
				switch(key) {
				case "diffuse":
				case "type":
					byteLen += 16;
					break;
				case "map":
				case "opacity":
				case "alphaTest":
				case "vertexColors":
				case "side":
				case "transparent":
				case "skinning":
					byteLen += 8;
					break;
				}

			}

			let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);
			let buffer = new ArrayBuffer(bufferLen);
			let view = new DataView(buffer);

			view.setUint8(0, "M".charCodeAt(0));
			view.setUint8(1, "A".charCodeAt(0));
			view.setUint8(2, "T".charCodeAt(0));
			view.setUint8(3, 0);
			view.setUint32(4, byteLen, true);
			
			let ofs = 8;
			for(let key in this.materials[i]) {
				
				let val = this.materials[i][key];

				switch(key) {
				case "diffuse":

					view.setUint8(ofs + 0, "D".charCodeAt(0));
					view.setUint8(ofs + 1, "I".charCodeAt(0));
					view.setUint8(ofs + 2, "F".charCodeAt(0));
					view.setUint8(ofs + 3, "F".charCodeAt(0));
					view.setFloat32(ofs + 4, val.r, true);
					view.setFloat32(ofs + 8, val.g, true);
					view.setFloat32(ofs + 12, val.b, true);
					ofs += 16;

					break;
				case "type":

					view.setUint8(ofs + 0, "T".charCodeAt(0));
					view.setUint8(ofs + 1, "Y".charCodeAt(0));
					view.setUint8(ofs + 2, "P".charCodeAt(0));
					view.setUint8(ofs + 3, "E".charCodeAt(0));
					ofs+=4;
					for(let k = 0; k < val.length; k++) {
						view.setUint8(ofs + k, val.charCodeAt(k));
					}
					ofs += 12;

					break;
				case "map":
					view.setUint8(ofs + 0, "M".charCodeAt(0));
					view.setUint8(ofs + 1, "A".charCodeAt(0));
					view.setUint8(ofs + 2, "P".charCodeAt(0));
					view.setUint8(ofs + 3, "\0".charCodeAt(0));
					view.setUint32(ofs + 4, val, true);
					ofs += 8;
					break;
				case "opacity":
					view.setUint8(ofs + 0, "O".charCodeAt(0));
					view.setUint8(ofs + 1, "P".charCodeAt(0));
					view.setUint8(ofs + 2, "A".charCodeAt(0));
					view.setUint8(ofs + 3, "C".charCodeAt(0));
					view.setFloat32(ofs + 4, val, true);
					ofs += 8;
					break;
				case "alphaTest":
					view.setUint8(ofs + 0, "A".charCodeAt(0));
					view.setUint8(ofs + 1, "L".charCodeAt(0));
					view.setUint8(ofs + 2, "P".charCodeAt(0));
					view.setUint8(ofs + 3, "T".charCodeAt(0));
					view.setFloat32(ofs + 4, val, true);
					ofs += 8;
					break;
				case "vertexColors":
					view.setUint8(ofs + 0, "V".charCodeAt(0));
					view.setUint8(ofs + 1, "C".charCodeAt(0));
					view.setUint8(ofs + 2, "L".charCodeAt(0));
					view.setUint8(ofs + 3, "R".charCodeAt(0));
					view.setUint32(ofs + 4, val, true);
					ofs += 8;
					break;
				case "side":
					view.setUint8(ofs + 0, "S".charCodeAt(0));
					view.setUint8(ofs + 1, "I".charCodeAt(0));
					view.setUint8(ofs + 2, "D".charCodeAt(0));
					view.setUint8(ofs + 3, "E".charCodeAt(0));
					view.setUint32(ofs + 4, val, true);
					ofs += 8;
					break;
				case "transparent":
					view.setUint8(ofs + 0, "T".charCodeAt(0));
					view.setUint8(ofs + 1, "R".charCodeAt(0));
					view.setUint8(ofs + 2, "N".charCodeAt(0));
					view.setUint8(ofs + 3, "S".charCodeAt(0));
					view.setUint32(ofs + 4, val, true);
					ofs += 8;
					break;
				case "skinning":
					view.setUint8(ofs + 0, "S".charCodeAt(0));
					view.setUint8(ofs + 1, "K".charCodeAt(0));
					view.setUint8(ofs + 2, "I".charCodeAt(0));
					view.setUint8(ofs + 3, "N".charCodeAt(0));
					view.setUint32(ofs + 4, val, true);
					ofs += 8;
					break;
				}
			}

			this.blocks.push({
				type : "MAT",
				id : i,
				num : properties.length,
				buffer: buffer
			});
			
		}

	},

	writeFaceGroups : function() {

		let structSize = 2;

		structSize += this.attrs.uv_count * 8;

		if(this.attrs.vertex_normals) {
			structSize += 12;
		}	

		if(this.attrs.vertex_colors) {
			structSize += 12;
		}	

		for(let i = 0; i < this.groups.length; i++) {
			
			let group = this.groups[i];

			let byteLen = group.tri.length * structSize;
			let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);

			let buffer = new ArrayBuffer(bufferLen);
			let view = new DataView(buffer);

			view.setUint8(0, 0x46); // 'F'
			view.setUint8(1, 0x41); // 'A'
			view.setUint8(2, 0x43); // 'C'
			view.setUint8(3, 0x45); // 'E'
			view.setUint32(4, byteLen, true);
			
			let ofs = 8;

			group.tri.forEach(tri => {
				
				view.setUint16(ofs, tri.index, true);
				ofs += 2;
				
				for(let k = 0; k < this.attrs.uv_count; k++) {
					view.setFloat32(ofs + 0, tri.uv[k].u, true);
					view.setFloat32(ofs + 4, tri.uv[k].v, true);
					ofs += 8;
				}

				if(this.attrs.vertex_normals) {
					view.setFloat32(ofs + 0, tri.normal.x, true);
					view.setFloat32(ofs + 4, tri.normal.y, true);
					view.setFloat32(ofs + 8, tri.normal.z, true);
					ofs += 12;
				}	

				if(this.attrs.vertex_colors) {
					tri.vcolor = tri.vcolor || { r : 1, g : 1, b : 1 };
					view.setFloat32(ofs + 0, tri.vcolor.r, true);
					view.setFloat32(ofs + 4, tri.vcolor.g, true);
					view.setFloat32(ofs + 8, tri.vcolor.b, true);
					ofs += 12;
				}	

			});

			this.blocks.push({
				type : "FACE",
				mat_id : group.matIndex,
				num : group.tri.length,
				buffer : buffer
			});

		}

	},

	writeThreeAnimations : function() {

		for(let i = 0; i < this.anims.length; i++) {
			
			let anim = this.anims[i];
			//let size = (anim.hierarchy.length*2 + 1) * 4;
			let size = anim.blockSize;
			
			/*
			for(let k = 0; k < anim.hierarchy.length; k++) {
				size += anim.hierarchy[k].keys.length * 68;
			}
			*/

			let byteLen = size;
			let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);
			let buffer = new ArrayBuffer(bufferLen);
			let view = new DataView(buffer);

			view.setUint8(0, 'A'.charCodeAt(0));
			view.setUint8(1, 'N'.charCodeAt(0));
			view.setUint8(2, 'I'.charCodeAt(0));
			view.setUint8(3, 'M'.charCodeAt(0));
			view.setUint32(4, byteLen, true);

			let ofs = 8;
			view.setFloat32(ofs, anim.length, true);
			ofs += 4;

			let num_frame = 0;
			let hierarchy = anim.hierarchy;
			for(let k = 0; k < hierarchy.length; k++) {
				
				view.setInt32(ofs, hierarchy[k].parent, true);
				view.setUint32(ofs + 4, hierarchy[k].keys.length, true);
				ofs += 8;

				for(let j = 0; j < hierarchy[k].keys.length; j++) {
					
					num_frame++;
					let frame = hierarchy[k].keys[j];
					let time = frame.time;
					let elements = frame.elements;

					view.setFloat32(ofs, time, true);
					ofs += 4;

					let str = "";

					if(frame.pos) {
						str += 'p';
					}

					if(frame.rot) {
						str += 'r';
					}

					if(frame.scl) {
						str += 's';
					}

					for(let n = 0; n < str.length; n++) {
						view.setUint8(ofs + n, str.charCodeAt(n));
					}
					ofs += 4;

					if(frame.pos) {
						view.setFloat32(ofs + 0, frame.pos[0], true);
						view.setFloat32(ofs + 4, frame.pos[1], true);
						view.setFloat32(ofs + 8, frame.pos[2], true);
						ofs += 12;
					}

					if(frame.rot) {
						view.setFloat32(ofs + 0, frame.rot[0], true);
						view.setFloat32(ofs + 4, frame.rot[1], true);
						view.setFloat32(ofs + 8, frame.rot[2], true);
						view.setFloat32(ofs + 12, frame.rot[3], true);
						ofs += 16;
					}

					if(frame.scl) {
						view.setFloat32(ofs + 0, frame.scl[0], true);
						view.setFloat32(ofs + 4, frame.scl[1], true);
						view.setFloat32(ofs + 8, frame.scl[2], true);
						ofs += 12;
					}

				}

			}

			this.blocks.push({
				type : "ANIM",
				id : i,
				num : num_frame,
				buffer, buffer
			});

		}

	},

	writeMatrixAnimations : function() {

		for(let i = 0; i < this.anims.length; i++) {
			
			let anim = this.anims[i];
			let size = (anim.hierarchy.length*2 + 1) * 4;
			
			for(let k = 0; k < anim.hierarchy.length; k++) {
				size += anim.hierarchy[k].keys.length * 68;
			}

			let byteLen = size;
			let bufferLen = THREE.DashExporter.calcBufferLen(byteLen);
			let buffer = new ArrayBuffer(bufferLen);
			let view = new DataView(buffer);

			view.setUint8(0, 'A'.charCodeAt(0));
			view.setUint8(1, 'N'.charCodeAt(0));
			view.setUint8(2, 'I'.charCodeAt(0));
			view.setUint8(3, 'M'.charCodeAt(0));
			view.setUint32(4, byteLen, true);

			let ofs = 8;
			view.setFloat32(ofs, anim.length, true);
			ofs += 4;

			let num_frame = 0;
			let hierarchy = anim.hierarchy;
			for(let k = 0; k < hierarchy.length; k++) {
				
				view.setInt32(ofs, hierarchy[k].parent, true);
				view.setUint32(ofs + 4, hierarchy[k].keys.length, true);
				ofs += 8;

				for(let j = 0; j < hierarchy[k].keys.length; j++) {
					
					num_frame++;
					let frame = hierarchy[k].keys[j];
					let time = frame.time;
					let elements = frame.elements;

					view.setFloat32(ofs, time, true);
					ofs += 4;

					elements.forEach(e => {

						view.setFloat32(ofs, e, true);
						ofs += 4;

					});

				}

			}

			this.blocks.push({
				type : "ANIM",
				id : i,
				num : num_frame,
				buffer, buffer
			});

		}

	},

	writeHeader : function() {

		let byteLen = (this.blocks.length+3) * 16;
		let gen_text = "Generated by THREE.DashExporter";

		let bufferLen = THREE.DashExporter.calcBufferLen(byteLen + gen_text.length);
		let buffer = new ArrayBuffer(bufferLen);
		let view = new DataView(buffer);

		view.setUint8(0, 'D'.charCodeAt(0));
		view.setUint8(1, 'M'.charCodeAt(0));
		view.setUint8(2, 'F'.charCodeAt(0));
		view.setUint8(3, '\0'.charCodeAt(0));
		view.setUint32(4, 32, true);
		view.setUint32(8, 1, true);
		view.setUint32(12, this.blocks.length, true);

		let ofs = 32;
		let startOfs = bufferLen;

		for(let i = 0; i < this.blocks.length; i++) {
			
			let block = this.blocks[i];
			switch(block.type) {
			case "ATTR":
				view.setUint8(ofs + 0, 'A'.charCodeAt(0));
				view.setUint8(ofs + 1, 'T'.charCodeAt(0));
				view.setUint8(ofs + 2, 'T'.charCodeAt(0));
				view.setUint8(ofs + 3, 'R'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "NAME":
				view.setUint8(ofs + 0, 'N'.charCodeAt(0));
				view.setUint8(ofs + 1, 'A'.charCodeAt(0));
				view.setUint8(ofs + 2, 'M'.charCodeAt(0));
				view.setUint8(ofs + 3, 'E'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "BONE":
				view.setUint8(ofs + 0, 'B'.charCodeAt(0));
				view.setUint8(ofs + 1, 'O'.charCodeAt(0));
				view.setUint8(ofs + 2, 'N'.charCodeAt(0));
				view.setUint8(ofs + 3, 'E'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "VERT":
				view.setUint8(ofs + 0, 'V'.charCodeAt(0));
				view.setUint8(ofs + 1, 'E'.charCodeAt(0));
				view.setUint8(ofs + 2, 'R'.charCodeAt(0));
				view.setUint8(ofs + 3, 'T'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "TEX":
				view.setUint8(ofs + 0, 'T'.charCodeAt(0));
				view.setUint8(ofs + 1, 'E'.charCodeAt(0));
				view.setUint8(ofs + 2, 'X'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 8, block.id, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "MAT":
				view.setUint8(ofs + 0, 'M'.charCodeAt(0));
				view.setUint8(ofs + 1, 'A'.charCodeAt(0));
				view.setUint8(ofs + 2, 'T'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 8, block.id, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "FACE":
				view.setUint8(ofs + 0, 'F'.charCodeAt(0));
				view.setUint8(ofs + 1, 'A'.charCodeAt(0));
				view.setUint8(ofs + 2, 'C'.charCodeAt(0));
				view.setUint8(ofs + 3, 'E'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 8, block.mat_id, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			case "ANIM":
				view.setUint8(ofs + 0, 'A'.charCodeAt(0));
				view.setUint8(ofs + 1, 'N'.charCodeAt(0));
				view.setUint8(ofs + 2, 'I'.charCodeAt(0));
				view.setUint8(ofs + 3, 'M'.charCodeAt(0));
				view.setUint32(ofs + 4, startOfs, true);
				view.setUint32(ofs + 8, block.id, true);
				view.setUint32(ofs + 12, block.num, true);
				break;
			}

			ofs += 16;
			startOfs += block.buffer.byteLength;
		}
		
		ofs+= 16;

		for(let i = 0; i < gen_text.length; i++) {
			view.setUint8(ofs+i, gen_text.charCodeAt(i));
		}

		this.blocks.unshift({
			type : "HEAD",
			buffer : buffer
		});

	},

	pack : function() {
		
		let array = new Array(this.blocks.length);

		for(let i = 0; i < this.blocks.length; i++) {
			array[i] = this.blocks[i].buffer;
		}

		return new Blob(array);

	}

}
