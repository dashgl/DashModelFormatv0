 # ***************************************************************************** #
 #
 # MIT License
 #
 # Copyright (c) 2018 Benjamin Collins (kion @ dashgl.com)
 #
 # Permission is hereby granted, free of charge, to any person obtaining a copy
 # of this software and associated documentation files (the "Software"), to deal
 # in the Software without restriction, including without limitation the rights
 # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 # copies of the Software, and to permit persons to whom the Software is
 # furnished to do so, subject to the following conditions:
 #
 # The above copyright notice and this permission notice shall be included in all
 # copies or substantial portions of the Software.
 #
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 # SOFTWARE.
 #
 # ***************************************************************************** #

import os
import bpy
import math
import bmesh
import tempfile

from struct import *
from mathutils import *
from os.path import basename
from bpy_extras.image_utils import load_image

class DashLoader:

	###########################################################################
	# Constructor                                                             #
	###########################################################################

	def __init__(self):
		
		self.blocks = {
			'ATTR' : [],
			'NAME' : [],
			'BONE' : [],
			'VERT' : [],
			'TEX' : [],
			'MAT' : [],
			'FACE' : [],
			'ANIM' : []
		}

		self.attrs = {
			'vertex_weight' : 0,
			'uv_count' : 0,
			'vertex_normals' : 0,
			'vertex_colors' : 0
		}

		self.names = {
			'bone' : [],
			'tex' : [],
			'anim' : []
		}

		self.bones = []
		self.verts = []
		self.faces = []
		self.texList = []
		self.matList = []
		self.anims = []
		return None

	###########################################################################
	# Parse                                                                   #
	###########################################################################

	def parse(self, filepath):
		
		# Set Mesh Name
		self.name = basename(filepath).split(".")[0]
		self.view = open(filepath, 'rb')
		
		# Read values from Dash Model File
		self.readHeader()
		self.readAttributes()
		self.readNames()
		self.readBones()
		self.readVertices()
		self.readTextures()
		self.readMaterials()
		self.readFaceGroups()
		self.readAnimations()
		
		# Create Objects to Link to Scene
		self.createBlenderObjects()

		return None

	###########################################################################
	# Read Header                                                             #
	###########################################################################

	def readHeader(self):
		
		data = self.view.read(16)
		s = unpack('4sIII', data)

		magic = s[0]
		ofs = s[1]
		version = s[2]
		nbBlocks = s[3]

		if magic != b'DMF\x00':
			raise ValueError('Magic number does not indicate DMF file')

		if version != 1:
			raise ValueError('Version not supported')

		self.view.seek(ofs);

		for i in range(0, nbBlocks):
			data = self.view.read(16)
			s = unpack('4sIII', data)
			
			if s[0] == b'ATTR':
				self.blocks['ATTR'].append(s)
			elif s[0] == b'NAME':
				self.blocks['NAME'].append(s)
			elif s[0] == b'BONE':
				self.blocks['BONE'].append(s)
			elif s[0] == b'VERT':
				self.blocks['VERT'].append(s)
			elif s[0] == b'TEX\x00':
				self.blocks['TEX'].append(s)
			elif s[0] == b'MAT\x00':
				self.blocks['MAT'].append(s)
			elif s[0] == b'FACE':
				self.blocks['FACE'].append(s)
			elif s[0] == b'ANIM':
				self.blocks['ANIM'].append(s)
		return None

	###########################################################################
	# Read Attributes                                                         #
	###########################################################################

	def readAttributes(self):
		
		if not len(self.blocks['ATTR']):
			return

		block = self.blocks['ATTR'][0]
		ofs = block[1]
		nbAttr = block[3]

		self.view.seek(ofs)
		data = self.view.read(8)
		s = unpack('4sI', data)

		if s[0] != b'ATTR':
			raise ValueError('Magic number does not indicate ATTR block')
		
		for i in range(0, nbAttr):
			data = self.view.read(8)
			s = unpack('4sI', data)

			if s[0] == b'VWGT':
				self.attrs['vertex_weight'] = s[1]
			elif s[0] == b'UVCT':
				self.attrs['uv_count'] = s[1]
			elif s[0] == b'VNRM':
				self.attrs['vertex_normals'] = s[1]
			elif s[0] == b'VCLR':
				self.attrs['vertex_colors'] = s[1]
		return None

	###########################################################################
	# Read Names                                                              #
	###########################################################################

	def readNames(self):

		for block in self.blocks['NAME']:
			ofs = block[1]
			nbName = block[3]
			self.view.seek(ofs)
			data = self.view.read(12)
			s = unpack('4sI4s', data)
			
			if s[0] != b'NAME':
				raise ValueError('Magic number does not indicate NAME block')

			type = s[2].decode('ascii')
			data = self.view.read(s[1])
			data = data.split(b'\x00')
			
			for i in range(0, nbName):
				self.names[type].append(data[i].decode("ascii"))
		return None

	###########################################################################
	# Read Bones                                                              #
	###########################################################################

	def readBones(self):
		
		if not len(self.blocks['BONE']):
			return

		block = self.blocks['BONE'][0]
		ofs = block[1]
		nbBone = block[3]

		self.view.seek(ofs)
		data = self.view.read(8)
		s = unpack('4sI', data)

		if s[0] != b'BONE':
			raise ValueError('Magic number does not indicate BONE block')
		
		for i in range(0, nbBone):
			bone = {}
			bone['id'] = unpack('h', self.view.read(2))[0]
			bone['parent_id'] = unpack('h', self.view.read(2))[0]
			bone['matrix']= unpack('ffffffffffffffff', self.view.read(64))
			self.bones.append(bone)
		return None

	###########################################################################
	# Read Vertices                                                           #
	###########################################################################

	def readVertices(self):
		
		if not len(self.blocks['VERT']):
			return

		block = self.blocks['VERT'][0]
		ofs = block[1]
		nbVert = block[3]

		self.view.seek(ofs)
		data = self.view.read(8)
		s = unpack('4sI', data)

		if s[0] != b'VERT':
			raise ValueError('Magic number does not indicate VERT block')
		
		for i in range(0, nbVert):
			vertex = {}
			vertex['index'] = i
			vertex['pos'] = unpack('fff', self.view.read(12))
			
			if self.attrs['vertex_weight']:
				vertex['indices'] = unpack('HHHH', self.view.read(8))
				vertex['weights'] = unpack('ffff', self.view.read(16))
			self.verts.append(vertex)

		return None

	###########################################################################
	# Read Textures                                                           #
	###########################################################################

	def readTextures(self):
		
		prefix = "data:image/PNG;base64,"

		for block in self.blocks['TEX']:
			ofs = block[1]
			texId = block[2]
			nbIndex = block[3]
			self.view.seek(ofs)
			data = self.view.read(8)
			s = unpack('4sI', data)
			
			print(s)
			if s[0] != b'TEX\x00':
				raise ValueError('Magic number does not indicate TEX block')
			
			tex = {}

			for i in range(0, nbIndex):

				magic = unpack('4s', self.view.read(4))[0]
				print(magic)

				if magic == b'TYPE':
					tex['type'] = unpack('4s', self.view.read(4))[0].decode('ascii')
				elif magic == b'LEN\x00':
					tex['len'] = unpack('I', self.view.read(4))[0]
				elif magic == b'FLPY':
					tex['flip_y'] = unpack('I', self.view.read(4))[0]
				elif magic == b'WID\x00':
					tex['width'] = unpack('I', self.view.read(4))[0]
				elif magic == b'HGT\x00':
					tex['height'] = unpack('I', self.view.read(4))[0]
				elif magic == b'WRPS':
					tex['wrap_s'] = unpack('I', self.view.read(4))[0]
				elif magic == b'WRPT':
					tex['wrap_t'] = unpack('I', self.view.read(4))[0]
				elif magic == b'IMG\x00':
					tex['img'] = self.view.read(tex['len'])
			
			with tempfile.TemporaryDirectory() as tmpdir:
				img_path = os.path.join(tmpdir, 'image_%d' % texId)
				with open(img_path, 'wb') as f:
					f.write(tex['img'])
				img = load_image(img_path)
				img.pack()
				img.file_format = "PNG"
				name = "texture_%03d" % texId
				texture = bpy.data.textures.new(name, type='IMAGE')
				texture.image = img
				print(texture)
				self.texList.append(texture)

		return None

	###########################################################################
	# Read Materials                                                          #
	###########################################################################

	def readMaterials(self):

		for block in self.blocks['MAT']:
			ofs = block[1]
			matId = block[2]
			nbIndex = block[3]
			self.view.seek(ofs)
			data = self.view.read(8)
			s = unpack('4sI', data)
			
			print(s)
			if s[0] != b'MAT\x00':
				raise ValueError('Magic number does not indicate MAT block')
			
			mat = {}
			
			for i in range(0, nbIndex):
				
				magic = unpack('4s', self.view.read(4))[0]

				if magic == b'TYPE':
					mat['type'] = unpack('12s', self.view.read(12))[0].decode('ascii')
				elif magic == b'DIFF':
					mat['diffuse'] = unpack('fff', self.view.read(12))
				elif magic == b'MAP\x00':
					mat['map'] = unpack('I', self.view.read(4))[0]
				elif magic == b'OPAC':
					mat['opacity'] = unpack('f', self.view.read(4))[0]
				elif magic == b'ALPT':
					mat['alphaTest'] = unpack('f', self.view.read(4))[0]
				elif magic == b'VCLR':
					mat['vertex_color'] = unpack('I', self.view.read(4))[0]
				elif magic == b'SIDE':
					mat['side'] = unpack('I', self.view.read(4))[0]
				elif magic == b'TRNS':
					mat['transparent'] = unpack('I', self.view.read(4))[0]
				elif magic == b'SKIN':
					mat['skinning'] = unpack('I', self.view.read(4))[0]

			self.matList.append(mat)

		return None

	###########################################################################
	# Read Face Groups                                                        #
	###########################################################################

	def readFaceGroups(self):

		for block in self.blocks['FACE']:
			ofs = block[1]
			matIndex = block[2]
			nbIndex = block[3]
			self.view.seek(ofs)
			data = self.view.read(8)
			s = unpack('4sI', data)
			
			if s[0] != b'FACE':
				raise ValueError('Magic number does not indicate FACE block')
			
			group = {}
			group['materialIndex'] = matIndex
			group['faceCount'] = int(nbIndex / 3)
			group['indices'] = []

			for i in range(0, nbIndex):
				indice = {}
				indice['index'] = unpack('H', self.view.read(2))[0]

				for k in range(0, self.attrs['uv_count']):
					indice['uv%d' % k] = unpack('ff', self.view.read(8))
					print(indice['uv%d' % k])
					u = indice['uv%d' % k][0]
					v = indice['uv%d' % k][1]
					indice['uv%d' % k] = ( u, -v )
		
				if self.attrs['vertex_normals']:
					indice['normal'] = unpack('fff', self.view.read(12))
		
				if self.attrs['vertex_colors']:
					indice['color'] = unpack('fff', self.view.read(12))
				
				group['indices'].append(indice)
			
			self.faces.append(group)

		return None

	###########################################################################
	# Read Animations                                                         #
	###########################################################################

	def readAnimations(self):

		for block in self.blocks['ANIM']:
			ofs = block[1]

			self.view.seek(ofs)
			data = self.view.read(12)
			s = unpack('4sIf', data)
			
			if s[0] != b'ANIM':
				raise ValueError('Magic number does not indicate FACE block')
			
			animation = {
				'fps' : 30,
				'length' : s[2],
				'hierarchy' : []
			}

			for bone in self.bones:
				parent_id = unpack('i', self.view.read(4))[0]
				num_keys = unpack('I', self.view.read(4))[0]

				hierarchy = {
					'parent' : parent_id,
					'keys' : []
				}

				for i in range(0, num_keys):
					
					frame = {
						'time' : unpack('f', self.view.read(4))[0] 
					}

					prs = unpack('4s', self.view.read(4))[0].decode("ASCII")
					
					if prs.find('p') != -1:
						frame['pos'] = unpack('fff', self.view.read(12))
					
					if prs.find('r') != -1:
						frame['rot'] = unpack('ffff', self.view.read(16))
					
					if prs.find('s') != -1:
						frame['scl'] = unpack('fff', self.view.read(12))

					hierarchy['keys'].append(frame)
				animation['hierarchy'].append(hierarchy)

			self.anims.append(animation)
		return None

	###########################################################################
	# Create bmesh                                                            #
	###########################################################################

	def createBlenderObjects(self):
		
		# Create Armature

		armature = bpy.data.armatures.new('Armature')
		armature_object=bpy.data.objects.new('Armature_object', armature)
		armature_object.show_x_ray=True
		armature.show_names=True
		#armature.draw_type = "STICK"
			
		bpy.context.scene.objects.link(armature_object)
		bpy.context.scene.objects.active = armature_object
		bpy.context.scene.update()

		bpy.ops.object.mode_set(mode='EDIT')
		
		mat_rot = Matrix.Rotation(math.radians(90.0), 4, 'Z')

		joints = []
		for bone in self.bones:
			id = bone['id']
			pid = bone['parent_id']
			mtx = bone['matrix']

			joint = armature.edit_bones.new("bone_%03d" % id)
			pos = Vector( (mtx[12], mtx[13], mtx[14]) )
			#pos = mat_rot * pos
			adjust = Vector( ( 0.0, 2.55, 0.0 )  )

			if pid == -1:
				joint.head = pos + adjust
				joint.tail = pos
			else:
				joint.parent = armature.edit_bones[pid]
				joint.tail = joint.parent.head + pos
				joint.head = joint.tail - adjust
			
			joints.append(joint)

		# Create Mesh

		bm = bmesh.new()
		mesh = bpy.data.meshes.new('Mesh')

		for vertex in self.verts:
			pos = vertex['pos']
			x = pos[0]
			y = pos[1]
			z = pos[2]
			p = Vector( (x, y, z) )
			#p = mat_rot * p
			vert = bm.verts.new( p ) 
		
		bm.verts.ensure_lookup_table()
		
		uv_list = []
		for group in self.faces:
			for i in range(0, group['faceCount']):
				a = bm.verts[group['indices'][i * 3 + 0]['index']]
				b = bm.verts[group['indices'][i * 3 + 1]['index']]
				c = bm.verts[group['indices'][i * 3 + 2]['index']]
				face = bm.faces.new( (a, b, c) )
				face.material_index = group['materialIndex']
				print(group['indices'][i * 3 + 0])
				uv_list.append(group['indices'][i * 3 + 0]['uv0'])
				uv_list.append(group['indices'][i * 3 + 1]['uv0'])
				uv_list.append(group['indices'][i * 3 + 2]['uv0'])

		uv_index = 0
		uv_layer = bm.loops.layers.uv.new()
		for face in bm.faces:
			for loop in face.loops:
				loop[uv_layer].uv = uv_list[uv_index]
				uv_index += 1


		bm.to_mesh(mesh)
		bm.free()
		mesh_object = bpy.data.objects.new('Mesh_Object', mesh)

		# Create Materials

		for i in range(0, len(self.matList)):
			mat = self.matList[i]
			material = bpy.data.materials.new("material_%03d" % i)

			if 'diffuse' in mat:
				material.diffuse_color.r = mat['diffuse'][0]
				material.diffuse_color.g = mat['diffuse'][1]
				material.diffuse_color.b = mat['diffuse'][2]

			#if 'transparent' in mat:
			#	material.use_transparent = mat['transparent']

			if 'opacity' in mat:
				material.alpha = mat['opacity']

			if 'map' in mat:
				print("need to map texture")
				mtex = material.texture_slots.add()
				texture = self.texList[mat['map']]
				mtex.texture = texture
				mtex.texture_coords = 'UV'
				mtex.use = True
				material.active_texture = texture

			mesh.materials.append(material)

		# Add Face Vetex UV's

		#layer_name = "uv_layer_000"
		#mesh.uv_textures.new(layer_name)
		#uv_layer = mesh.uv_layers[layer_name].data
		
		#num = 0
		#for group in self.faces:
		#	for i in range(0, group['faceCount']):
		#		uv_layer[num + 0].uv = group['indices'][i * 3 + 0]['uv0']
		#		uv_layer[num + 1].uv = group['indices'][i * 3 + 0]['uv0']
		#		uv_layer[num + 2].uv = group['indices'][i * 3 + 0]['uv0']
		#		num += 3
		



		# For Vertex Weights, first we create a group for each bone

		for i in range(0, len(self.bones)):
			mesh_object.vertex_groups.new("bone_%03d" % i)

		#Loop over all of the vertices
		for vertex in self.verts:
			index = vertex['index']
			indices = vertex['indices']
			weights = vertex['weights']
			mesh_object.vertex_groups[indices[0]].add([index], 1.0, "ADD")
		
		bpy.ops.object.mode_set(mode='OBJECT')
		
		mesh_object.parent = armature_object
		modifier = mesh_object.modifiers.new(type='ARMATURE', name="Armature")
		modifier.object = armature_object

		bpy.context.scene.objects.link(mesh_object)
		bpy.context.scene.objects.active = mesh_object

		# Create Animtions
		if len(self.anims) == 0:
			return None

		armature_object.animation_data_create()
		action = bpy.data.actions.new(name="anim_000")
		armature_object.animation_data.action = action

		anim = self.anims[1]
		num_frames = int(anim['length'] * anim['fps'] + 0.5)

		for i in range(0, len(anim['hierarchy'])):
			bone = self.bones[i]
			hierarchy = anim['hierarchy'][i]
			
			
			location = []
			rotation = []

			for frame in hierarchy['keys']:
				time = int(frame['time'] * anim['fps'] + 0.5)
				
				if 'pos' in frame:
					pos = frame['pos']
					#pos = Vector( (0.0, 0.0, 0.0) )
					location.append( (time, pos) )

				if 'rot' in frame:
					r = frame['rot']
					rot = Quaternion( (r[3], r[0], r[1], r[2]) )
					rotation.append( (time, rot) )

				if 'scl' in frame:
					scl = frame['scl']
			
			if i == 0:
				for axis_i in range(3):
					data_path = 'pose.bones["bone_%03d"].location' % i
					curve = action.fcurves.new(data_path=data_path, index=axis_i)
					keyframe_points = curve.keyframe_points
					keyframe_points.add(len(location))
				
					for frame_i in range(len(location)):
						keyframe_points[frame_i].co = (location[frame_i][0], location[frame_i][1][axis_i])
			
			for axis_i in range(4):
				data_path = 'pose.bones["bone_%03d"].rotation_quaternion' % i
				curve = action.fcurves.new(data_path=data_path, index=axis_i)
				keyframe_points = curve.keyframe_points
				keyframe_points.add(len(rotation))
				
				for frame_i in range(len(rotation)):
					keyframe_points[frame_i].co = (rotation[frame_i][0], rotation[frame_i][1][axis_i])


		return None
