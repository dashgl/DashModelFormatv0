# --------------------------------------------------------------------------- #
# Blender Addon : Dash Model Format (Import - Export)
# MIT License
# Copyright 2018 kion @ dashgl.com 
# --------------------------------------------------------------------------- #

bl_info = {
	"name": "Dash Model Format",
	"author": "Kion",
	"blender" : (2, 79, 0),
	"location": "File > Import-Export",
	"description": "Import-Export .dmf meshes",
	"warning" : "",
	"wiki_url" : "https://gitlab.com/kion-dgl/DashModelFormat",
	"category": "Import-Export",
}

import bpy
from bpy.types import Operator
from bpy_extras.io_utils import ExportHelper, ImportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty

# --------------------------------------------------------------------------- #
# Register Import and Export Menu Items
# --------------------------------------------------------------------------- #

def register():
	bpy.utils.register_module(__name__)
	bpy.types.INFO_MT_file_import.append(ImportDashModelFormat.menu_func)
	bpy.types.INFO_MT_file_export.append(ExportDashModelFormat.menu_func)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_import.remove(ImportDashModelFormat.menu_func)
	bpy.types.INFO_MT_file_export.remove(ExportDashModelFormat.menu_func)

if __name__ == "__main__":
	register()

# --------------------------------------------------------------------------- #
# Import Dash Model Format (.dmf)
# --------------------------------------------------------------------------- #

class ImportDashModelFormat(Operator, ImportHelper):
	
	bl_idname = "import.dmf"
	bl_label = "Import Dash Model Format"

	filename_ext = ".dmf"
	filter_glob = StringProperty(default="*.dmf", options={'HIDDEN'})
	
	@staticmethod
	def menu_func(self, context):
		self.layout.operator("import.dmf", text="Dash Model Format (.dmf)")

	def execute(self, context):
		from . import import_dmf
		loader = import_dmf.DashLoader()
		loader.parse(self.filepath)

		print("Hello, World!!!")
		return {'FINISHED'}
		
# --------------------------------------------------------------------------- #
# Export Dash Model Format (.dmf)
# --------------------------------------------------------------------------- #

class ExportDashModelFormat(bpy.types.Operator, ExportHelper):

	bl_idname = "export.dmf"
	bl_label = "Export Dash Model Format"

	filename_ext = ".dmf"

	@staticmethod
	def menu_func(self, context):
		default_path = bpy.data.filepath.replace(".blend", ".dmf")
		opts = self.layout.operator("export.dmf", text="Dash Model Format (.dmf)")
		opts.filepath = default_path

	def execute(self, context):
		from . import import_dmf
		exporter = import_dmf.DashExporter()
		export.parse(self.filepath)

		print("Goodbye, World!!!")
		return {'FINISHED'}

# --------------------------------------------------------------------------- #
# End Dash Model Format Import - Export Addon
# --------------------------------------------------------------------------- #
